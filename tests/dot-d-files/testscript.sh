TRAIN=$1

echo "Building"
${TRAIN}

timestamp_a=`stat -c %y "build/dummypkg/bin/a"`
timestamp_b=`stat -c %y "build/dummypkg/bin/b"`

echo "a: ${timestamp_a}"
echo "b: ${timestamp_b}"

echo "Update a header"
touch src/header.hpp

echo "Rebuilding"
${TRAIN}

timestamp_a_2=`stat -c %y "build/dummypkg/bin/a"`
timestamp_b_2=`stat -c %y "build/dummypkg/bin/b"`

if [ "${timestamp_a}" \!= "${timestamp_a_2}" ];
then
    echo "1/2 PASS: updated binary after updating header"
else
    echo "1/2 FAILED: binary not updated after header update"
fi

if [ "${timestamp_b}" = "${timestamp_b_2}" ];
then
    echo "2/2 PASS: unrelated binary not updated"
else
    echo "2/2 FAILED: unrelated binary updated"
fi

