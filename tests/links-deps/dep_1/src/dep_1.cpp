#include "include/dep_1.hpp"

namespace dep1
{
    int f(int val)
    {
        return (val+1);
    }
} // namespace dep1
