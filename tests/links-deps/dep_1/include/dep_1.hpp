#ifndef DEP1_DEP1_HPP
#define DEP1_DEP1_HPP

namespace dep1
{

    int f(int);

} // namespace dep1

#endif /* DEP1_DEP1_HPP */
