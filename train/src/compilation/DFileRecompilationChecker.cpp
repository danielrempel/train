#include "DFileRecompilationChecker.hpp"

#include <numeric>

#include <fmt/format.hpp>
#include <fmt/ostream.hpp>

#include <DFileParser/DFileParser.hpp>
#include "BasicRecompilationChecker.hpp"

/* ========================================================================= */
bool DFileRecompilationChecker::shouldRecompile(CompilationStep const& step) const
{
    /*
     * TODO: check if a depfile exists and gather deps if it does
     * .d files should be able to supplement all the deps, right?
     *
     * This recompilation checker should only target CC and CXX steps
     * Others should be delegated to the basic one.
     */

    if ("CC" == step.processor->name() or
            "CXX" == step.processor->name())
    {
        auto depfile = step.destination;
        depfile.replace_extension(".d");

        fmt::print("{}: {:03d}: {}(): file \"{}\", trying dependency \"{}\"\n",
                __FILE__, __LINE__, __func__,
                step.destination.string(),
                depfile.string()
            );

        if (not fs::exists(depfile))
        {
            // resort to basic algo
            goto basic_algo;
        }

        fmt::print("DFileRecompilationChecker: target = \"{}\"\n", step.destination.string());

        std::list<fs::path> deps;

        try
        {
            deps = DFileParser(depfile, step.destination.string()).GetDeps();
        }
        catch (std::exception const& e)
        {
            fmt::print("DFileRecompilationChecker: DFileParser failed: {}", e.what());
            fmt::print("DFileRecompilationChecker: definitely should rebuild");
            return true;
        }

        std::string to_print;
        for (auto const& file : step.sources)
        {
            to_print += ", " + file.string();
        }

        fmt::print("Sources: {}\n", to_print);

        to_print = "";
        for (auto const& file : deps)
        {
            to_print += ", " + file.string();
        }
        fmt::print("DFile: {}\n", to_print);

        return (BasicRecompilationChecker::shouldRecompile(step.destination,
                    deps));
    }

basic_algo:
    return (BasicRecompilationChecker::shouldRecompile(step.destination,
                step.sources));
}
