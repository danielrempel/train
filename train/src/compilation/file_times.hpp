#ifndef TRAIN_FILE_TIMES_HPP
#define TRAIN_FILE_TIMES_HPP

#include <chrono>
#include <filesystem>

namespace file_times
{
using time_point = std::chrono::system_clock::time_point;

time_point last_access_time(std::filesystem::path const& file);

time_point last_modification_time(std::filesystem::path const& file);

time_point last_attrib_time(std::filesystem::path const& file);

} // namespace file_times

#endif /* TRAIN_FILE_TIMES_HPP */
