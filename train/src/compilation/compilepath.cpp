#include <algorithm>

#include <fmt/format.hpp>

#include "compilepath.hpp"

/* ========================================================================= */
class CompilationPathNotFound : public std::exception
{
public:
    virtual ~CompilationPathNotFound() noexcept
    {}
};

/* ========================================================================= */
void RecursivelyFindCompilationPath(std::list<std::string>& search_stack,
        std::list<Processor const*>& resulting_path,
        std::list<Processor> const& processors,
        std::string const& destination_type)
{
    if (destination_type == search_stack.back())
    {
       return;
    }

    for (auto const& proc : processors)
    {
        //fmt::print("try processor {}: src={}, looking for={}\n",
        //        proc.name, proc.source_type(), search_stack.back());
        if (proc.source_type() == search_stack.back())
        {
            search_stack.push_back(proc.dest_type());
            resulting_path.push_back(&proc);
            try
            {
                RecursivelyFindCompilationPath(search_stack, resulting_path,
                        processors, destination_type);
                return;
            }
            catch (CompilationPathNotFound const& not_found)
            {
                search_stack.pop_back();
                resulting_path.pop_back();
            }
        }
    }

    throw CompilationPathNotFound();
}

void BreakCompilationPathsIntoBuckets(
        std::map<std::string, std::list<Processor const*>> const& paths,
        std::vector<std::vector<Processor const*>>& result_buckets
    )
{
    if (paths.empty())
    {
        return;
    }

    std::vector<std::size_t> lengths;
    lengths.reserve(paths.size());

    for (auto kv : paths)
    {
        fmt::print("Path: ");
        for (Processor const* processor : kv.second)
        {
            fmt::print("{}, ", processor->source_type());
        }
        fmt::print("\n");
    }

    std::transform(paths.begin(), paths.end(),
            std::inserter(lengths, lengths.end()),
            [](auto const& kv) -> std::size_t
            {
                return kv.second.size();
            });

    std::size_t max = *std::max_element(lengths.begin(), lengths.end());

    fmt::print("Max depth : {}\n", max);

    std::vector<std::vector<Processor const*>> buckets(max);

    for (auto const& kv : paths)
    {
        std::size_t depth = max;
        for (auto iter = kv.second.rbegin(); iter != kv.second.rend(); ++iter)
        {
            --depth;

            buckets[depth].push_back(*iter);
        }
    }

    for (auto& bucket : buckets)
    {
        std::sort(bucket.begin(), bucket.end());

        bucket.erase(std::unique(bucket.begin(), bucket.end()), bucket.end());
    }

    for (auto const& bucket : buckets)
    {
        fmt::print("Bucket: ");

        for (Processor const* processor : bucket)
        {
            fmt::print("{}, ", processor->source_type());
        }

        fmt::print("\n");
    }

    result_buckets = std::move(buckets);
}

/* ========================================================================= */
void ReorderStepsPerBuckets(
        std::list<CompilationStep>& steps,
        std::vector<std::vector<Processor const*>> const& order_buckets
    )
{
    auto left_iter = steps.begin();
    auto right_iter = steps.end();

    for (auto const& bucket : order_buckets)
    {
        for (Processor const* processor : bucket)
        {
            auto new_left =
                std::partition(left_iter, right_iter,
                        [=](CompilationStep const& step) -> bool
                        {
                            return (step.processor->source_type() == processor->source_type());
                        }
                    );
            left_iter = new_left;
        }
    }
}

/* ========================================================================= */
void CompileObjects(fs::path const& source_dir,
        fs::path const& build_dir,
        std::list<fs::path> const& sources,
        std::map<std::string, std::list<Processor const*>> const& paths,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_files)
{
    /* Sort processors into buckets based on their depths in the compilation
     * paths. (More or less)
     * The required result:
     * .yy -> .cpp -> .o
     * .cpp -> .o
     *
     * Buckets: [ .yy ], [ .cpp ], [ .o ]
     *
     * Intent: make sure that translation of other formats into .cpp (+.hpp)
     * happens before the compilation of the rest of .cpp sources - alleviates
     * the need to explicitly define that .yy -> .cpp also produces .hpp that
     * someone might depend on.
     * Not sure it's the best way of doing stuff :)
     */
    std::vector<std::vector<Processor const*>> order_buckets;
    BreakCompilationPathsIntoBuckets(paths, order_buckets);

    std::list<CompilationStep> append_steps;
    std::list<fs::path> append_files;

    for (auto const& file : sources)
    {
        auto ext = file.extension().string();

        // find the appropriate path in the given list
        std::list<Processor const*> const& compilation_path_base = paths.at(ext);
        std::list<Processor const*> compilation_path = compilation_path_base;

        // Handle the first compilation step is special ======================
        // src/???.cpp -> build/???.o
        fs::path destination_file = build_dir / file;
        destination_file.replace_extension(compilation_path.front()->dest_type());

        append_steps.push_back(CompilationStep {
                { source_dir / file },
                destination_file,
                compilation_path.front(),
                source_dir / file
            });

        // Drop the first path node - we already queued it
        compilation_path.pop_front();

        // Handle the rest of the compilation steps ==========================
        fs::path temp_file = destination_file;
        for (auto const* proc : compilation_path)
        {
            fs::path product = temp_file;
            product.replace_extension(proc->dest_type());

            append_steps.push_back(CompilationStep {
                    { temp_file },
                    product,
                    proc,
                    source_dir / file, /* origin */
                });

            temp_file = product;
        }

        append_files.push_back(temp_file);
    }

    ReorderStepsPerBuckets(append_steps, order_buckets);

    result_steps.splice(result_steps.end(), std::move(append_steps));
    result_files.splice(result_files.end(), std::move(append_files));
}

/* ========================================================================= */
Processor const&
FindProcessor(std::list<Processor> const& processors,
        std::string const& name)
{
    auto iter = std::find_if(processors.cbegin(),
            processors.cend(),
            [&](Processor const& p) -> bool
            {
                return (p.name() == name);
            });
    if (processors.cend() == iter)
    {
        throw std::runtime_error("Required processor not found");
    }

    return (*iter);
}
