#include <sys/stat.h>
#include <sys/time.h>

#include <system_error>
#include <cerrno>

#include "file_times.hpp"

namespace file_times
{
inline std::system_error from_errno(int saved_errno)
{
    return (std::system_error(
                std::error_code(saved_errno, std::system_category())));
}

time_point last_access_time(std::filesystem::path const& file)
{
    struct stat buffer = { 0 };

    if (0 != stat(file.string().c_str(), &buffer))
    {
        throw from_errno(errno);
    }

    struct timespec atime = buffer.st_atim;

    return (std::chrono::system_clock::time_point(
                std::chrono::seconds(atime.tv_sec) +
                std::chrono::nanoseconds(atime.tv_nsec)));
}

time_point last_modification_time(std::filesystem::path const& file)
{
    struct stat buffer = { 0 };

    if (0 != stat(file.string().c_str(), &buffer))
    {
        throw from_errno(errno);
    }

    struct timespec mtime = buffer.st_mtim;

    return (std::chrono::system_clock::time_point(
                std::chrono::seconds(mtime.tv_sec) +
                std::chrono::nanoseconds(mtime.tv_nsec)));
}

time_point last_attrib_time(std::filesystem::path const& file)
{
    struct stat buffer = { 0 };

    if (0 != stat(file.string().c_str(), &buffer))
    {
        throw from_errno(errno);
    }

    struct timespec ctime = buffer.st_ctim;

    return (std::chrono::system_clock::time_point(
                std::chrono::seconds(ctime.tv_sec) +
                std::chrono::nanoseconds(ctime.tv_nsec)));
}

} // namespace file_times
