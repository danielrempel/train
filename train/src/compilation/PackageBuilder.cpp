#include "PackageBuilder.hpp"

#include <functional>

#include <fmt/format.hpp>
#include <fmt/ostream.hpp>

#include "compilepath.hpp"
#include "targets.hpp"

#include "../flag_transform_callback.hpp"

#include "DFileRecompilationChecker.hpp"

/* ========================================================================= */
PackageBuilder::PackageBuilder(Workspace& workspace, Package& package)
    : m_workspace(workspace), m_package(package),
        m_processors(m_package.custom_processors())
{
    std::list<Processor> procs_custom_copy(m_workspace.base_processors());

    m_processors.splice(m_processors.end(), std::move(procs_custom_copy));

    m_deps_checker.reset(new DFileRecompilationChecker());
}

/* ========================================================================= */
void PackageBuilder::setBinaryPkgLinkDeps(std::list<std::string>&& flags)
{
    /*
     * find LD
     * append flags
     */
    auto iter = std::find_if(m_processors.begin(),
            m_processors.end(),
            [&](Processor const& p) -> bool
            {
                return (p.name() == "LD");
            });

    assert(m_processors.end() != iter);

    iter->appendFlags(std::move(flags));
}

/* ========================================================================= */
void PackageBuilder::build()
{
    m_package.gatherSourceFiles();

    std::list<CompilationStep> steps;
    std::list<fs::path> result_objects;

    switch (m_package.type())
    {
        case Package::Type::Bin:
            fmt::print("Target: all binaries\n");
            TargetAllBinaries(m_package.abs_source_dir(),
                    m_package.abs_build_dir(), m_package.other_sources(),
                    m_package.binary_sources(), m_processors,
                    m_package.name(), steps, result_objects);
            break;
        case Package::Type::Lib:
            fmt::print("Target: static lib\n");
            TargetStaticLib(m_package.abs_source_dir(),
                    m_package.abs_build_dir(), m_package.other_sources(),
                    m_package.binary_sources(), m_processors,
                    m_package.name(), steps, result_objects);

            TargetSymlinkIncludeDir(m_package.abs_package_dir(),
                    m_workspace.rel_include_dir(),
                    m_workspace.abs_build_base_dir(), m_package.other_sources(),
                    m_package.binary_sources(), m_processors, m_package.name(),
                    steps, result_objects);
            break;
        case Package::Type::SO:
            fmt::print("Target: shared lib\n");
            TargetSharedLib(m_package.abs_source_dir(), m_package.abs_build_dir(),
                    m_package.other_sources(), m_package.binary_sources(),
                    m_processors, m_package.name(), steps, result_objects);

            TargetSymlinkIncludeDir(m_package.abs_package_dir(),
                    m_workspace.rel_include_dir(),
                    m_workspace.abs_build_base_dir(), m_package.other_sources(),
                    m_package.binary_sources(), m_processors, m_package.name(),
                    steps, result_objects);
            break;
        default:
            fmt::print("{}: {:03d}: wrong package type\n", __FILE__, __LINE__);
            throw std::runtime_error("Wrong package type");
    }

    /* ===================================================================== */
    gatherDirectories(m_processors, steps);

    auto flag_transform_callback = std::bind(FlagTransformCallback,
            std::cref(m_package.abs_package_dir()),
            std::cref(m_workspace.abs_build_include_dir()),
            std::cref(m_package.abs_build_dir()),
            std::placeholders::_1);

    executePlan(steps, flag_transform_callback, *m_deps_checker);
}

/* ========================================================================= */
/*static*/ void PackageBuilder::applyPerFileFlags(Processor const& processor,
    fs::path const& origin_file, std::list<std::string>& result_flags)
{
    if (processor.hasAddition(origin_file))
    {
        Processor::Additions const& adds = processor.additions(origin_file);
        for (auto const& add : adds.add_flags)
        {
            result_flags.push_back(add);
        }

        for (auto const& remove : adds.remove_flags)
        {
            result_flags.remove(remove);
        }

        result_flags.sort();
        result_flags.unique();
    }
}

/* ========================================================================= */
/*static*/ void
    PackageBuilder::executePlan(std::list<CompilationStep> const& steps,
        std::function<std::string(std::string const&)> flag_transform,
        IRecompilationChecker& deps_checker)
{
    for (auto const& step : steps)
    {
        if (not deps_checker.shouldRecompile(step))
        {
            continue;
        }

        std::string sources;
        for (auto const& source : step.sources)
        {
            sources += fmt::format("{} ", source);
        }

        std::list<std::string> flags_list(step.processor->flags());

        if ("" != step.origin_file)
        {
            applyPerFileFlags(*step.processor, step.origin_file,
                flags_list);
        }

        std::string flags_pat;
        for (auto const& flag : flags_list)
        {
            flags_pat += fmt::format("{} ", flag);
        }

        std::string flags = flag_transform(flags_pat);

        std::string command_line = fmt::format(step.processor->format_string(),
                fmt::arg("command", step.processor->command()),
                fmt::arg("input", sources),
                fmt::arg("output", step.destination),
                fmt::arg("flags", flags));

        fmt::print("{}\n", command_line);

        if (0 != std::system(command_line.c_str()))
        {
            fmt::print("Command failed!\n");
            throw (std::runtime_error("command failed"));
        }
    }
}

/* ========================================================================= */
/*static*/ void
    PackageBuilder::gatherDirectories(std::list<Processor> const& processors,
        std::list<CompilationStep>& steps)
{
    std::list<fs::path> build_directories;
    for (auto const& step : steps)
    {
        fs::path directory = step.destination.parent_path().lexically_normal();

        if (build_directories.end() == std::find(build_directories.begin(),
                    build_directories.end(), directory))
        {
            build_directories.push_back(directory);
        }
    }
    build_directories.sort();

    Processor const& mkdir = FindProcessor(processors, "MKDIR");

    while (build_directories.size())
    {
        CompilationStep step
        {
            {},
            build_directories.back(),
            &mkdir
        };

        steps.push_front(std::move(step));
        build_directories.pop_back();
    }
}
