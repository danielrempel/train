#include "BasicRecompilationChecker.hpp"

#include <fmt/format.hpp>
#include <fmt/ostream.hpp>

#include "../common_header.hpp"
#include "file_times.hpp"

/* ========================================================================= */
bool BasicRecompilationChecker::shouldRecompile(CompilationStep const& step) const
{
    return (shouldRecompile(step.destination, step.sources));
}

/* ========================================================================= */
/*static*/ bool BasicRecompilationChecker::shouldRecompile(fs::path const& object,
        std::list<fs::path> const& deps)
{
    // dependencies
    // 1. check if the target already exists
    bool exists = fs::exists(object);

    // 2. get the newest source files' timestamp
    file_times::time_point destination_time; // default construct to zero
    if (exists)
    {
        destination_time = file_times::last_modification_time(object);
    }

    file_times::time_point newest_source; // default construct it to zero
    for (auto const& source : deps)
    {
        auto time = file_times::last_modification_time(source);

        if (time > newest_source)
        {
            newest_source = time;
        }
    }

    // 3. no need to rebuild anyways
    if (exists and 0 == deps.size())
    {
        fmt::print("object {} exists and has no dependencies\n",
                object);

        // skip to next object
        return false;
    }

    // 3. compare that timestamp with the target's one
    if (destination_time > newest_source)
    {
        fmt::print("object {} exists and is newer than its source(s)\n",
                object);

        // skip to next object
        return false;
    }

    return true;
}
