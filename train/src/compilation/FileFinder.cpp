#include "FileFinder.hpp"

#include <algorithm>
#include <fmt/format.hpp>

namespace fs = std::filesystem;

/* bug note 1: as of now, std::filesystem::path comparison is lexicographical
 * and there's no way to remove a trailing slash that is left by
 * path::remove_filename(), so to compare directory names, you have to manually
 * make sure that you construct a path ending in a slash
 */
/* bug note 2:
 * fs::path("a").operator std::string() -> "\"a\""
 * fs::path("a").string() -> "a"
 */

FileFinder::FileFinder(std::filesystem::path const& source_folder,
            std::list<std::string> const& base_source_types,
            std::list<std::string> const& custom_source_types,
            std::list<std::filesystem::path>& binary_sources,
            std::list<std::filesystem::path>& other_sources)
    : m_source_folder(source_folder), m_binary_sources(binary_sources),
        m_other_sources(other_sources),
        m_bin_dir((source_folder / "bin/").lexically_normal()) /* bug note 1 */
{
    for (auto const& type : base_source_types)
    {
        /* must explicitly convert fs::path to string, see bug note 2 */
        std::string name = fmt::format("main{}", type);

        m_possible_mains.push_back((m_source_folder / name).lexically_normal());
    }
    for (auto const& type : custom_source_types)
    {
        /* must explicitly convert fs::path to string, see bug note 2 */
        std::string name = fmt::format("main{}", type);

        m_possible_mains.push_back((m_source_folder / name).lexically_normal());
    }

    for (auto const& type : base_source_types)
    {
        auto pat = fmt::format(R"(\{}$)", type);
        m_regexes.push_back(std::regex(pat));
    }
    for (auto const& type : custom_source_types)
    {
        auto pat = fmt::format(R"(\{}$)", type);
        m_regexes.push_back(std::regex(pat));
    }
}

void FileFinder::Gather()
{
    if (not fs::exists(m_source_folder))
    {
        return; // OK. no files to gather
    }

    if (not fs::is_directory(m_source_folder))
    {
        // expected it to be a directory!
        fmt::print("FileFinder::Gather(): expected {} to be a directory!\n",
                m_source_folder.string());

        throw std::runtime_error("Predefined source directory path is taken "
                "by a file");
    }

    for (auto it = fs::recursive_directory_iterator(m_source_folder);
            it != fs::recursive_directory_iterator();
            ++it)
    {
        if (it.depth() > RECURSIVE_DESCENT_MAX_DEPTH)
        {
            fmt::print(stderr, "recursive descent: too deep\n");
            throw std::runtime_error("File tree too deep");
        }

        this->Visit(*it);
    }

    m_binary_sources.sort();
    m_other_sources.sort();
}

void FileFinder::Visit(fs::directory_entry const& entry)
{
    bool is_file = entry.is_regular_file();

    //fmt::print("FileFinder::Visit(): {}\n", entry.path().string());

    if (not is_file)
    {
        return;
    }

    for (auto const& pattern : m_regexes)
    {
        if (std::regex_search(entry.path().string(), pattern))
        {
            goto file_is_source;
        }
    }
    return; // the entry is not a source code file
    file_is_source:

    for (auto const& possible_main : m_possible_mains)
    {
        if (entry.path().lexically_normal() == possible_main)
        {
            m_binary_sources.push_back(entry.path().lexically_relative(m_source_folder));
            return;
        }
    }

    /* bug note 1 */
    if (fs::path(entry.path()).remove_filename().lexically_normal() == m_bin_dir)
    {
        m_binary_sources.push_back(entry.path().lexically_relative(m_source_folder));
        return;
    }

    m_other_sources.push_back(entry.path().lexically_relative(m_source_folder));
}
