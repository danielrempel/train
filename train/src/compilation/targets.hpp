#ifndef TRAIN_TARGETS_HPP
#define TRAIN_TARGETS_HPP

#include <list>
#include <filesystem>
namespace fs = std::filesystem;

#include "compilepath.hpp"

void TargetStaticLib(fs::path const& source_dir,
        fs::path const& build_dir, std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects);

void TargetAllBinaries(fs::path const& source_dir,
        fs::path const& build_dir, std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects);

void TargetSymlinkIncludeDir(fs::path const& abs_package_dir,
        fs::path const& rel_include_dir,
        fs::path const& abs_global_build_dir,
        std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects);

void TargetSharedObjects(fs::path const& source_dir,
        fs::path const& build_dir, std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects);

void TargetSharedLib(fs::path const& source_dir,
        fs::path const& build_dir, std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects);

#endif /* TRAIN_TARGETS_HPP */
