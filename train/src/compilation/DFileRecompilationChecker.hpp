#ifndef TRAIN_COMPILATION_DFILERECOMPILATIONCHECKER_HPP
#define TRAIN_COMPILATION_DFILERECOMPILATIONCHECKER_HPP

class DFileRecompilationChecker;

#include "IRecompilationChecker.hpp"

class DFileRecompilationChecker : public IRecompilationChecker
{
public:
    DFileRecompilationChecker() = default;
    virtual ~DFileRecompilationChecker() noexcept = default;

    bool shouldRecompile(CompilationStep const&) const override;
};

#endif /* TRAIN_COMPILATION_DFILERECOMPILATIONCHECKER_HPP */
