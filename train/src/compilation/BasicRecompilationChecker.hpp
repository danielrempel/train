#ifndef TRAIN_COMPILATION_BASICRECOMPILATIONCHECKER_HPP
#define TRAIN_COMPILATION_BASICRECOMPILATIONCHECKER_HPP

#include "IRecompilationChecker.hpp"

class BasicRecompilationChecker : public IRecompilationChecker
{
public:
    BasicRecompilationChecker() = default;
    virtual ~BasicRecompilationChecker() noexcept = default;

    bool shouldRecompile(CompilationStep const&) const override;

    /* Provided for reuse */
    static bool shouldRecompile(fs::path const& object,
            std::list<fs::path> const& deps);
};

#endif /* TRAIN_COMPILATION_BASICRECOMPILATIONCHECKER_HPP */
