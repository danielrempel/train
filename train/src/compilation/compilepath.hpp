#ifndef TRAIN_COMPILEPATH_HPP
#define TRAIN_COMPILEPATH_HPP

#include <list>
#include <map>
#include <filesystem>
namespace fs = std::filesystem;

#include "../entities/CompilationStep.hpp"

void CompileObjects(fs::path const& source_dir,
        fs::path const& build_dir,
        std::list<fs::path> const& sources,
        std::map<std::string, std::list<Processor const*>> const& paths,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_files);

void RecursivelyFindCompilationPath(std::list<std::string>& search_stack,
        std::list<Processor const*>& resulting_path,
        std::list<Processor> const& processors,
        std::string const& destination_type);

Processor const&
FindProcessor(std::list<Processor> const& processors, std::string const& name);

#endif /* TRAIN_COMPILEPATH_HPP */
