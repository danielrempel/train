#ifndef TRAIN_COMPILATION_PACKAGEBUILDER_HPP
#define TRAIN_COMPILATION_PACKAGEBUILDER_HPP

#include <list>
#include <string>
#include <memory>

#include "../common_header.hpp"
#include "../entities/Package.hpp"
#include "../entities/Workspace.hpp"
#include "../entities/CompilationStep.hpp"
#include "IRecompilationChecker.hpp"

class PackageBuilder final
{
public:
    PackageBuilder(Workspace&, Package&);
    ~PackageBuilder() noexcept = default;

    void setBinaryPkgLinkDeps(std::list<std::string>&& flags);
    void build();

private:
    Workspace& m_workspace;
    Package& m_package;

    std::list<Processor> m_processors;

    std::unique_ptr<IRecompilationChecker> m_deps_checker;

    static void applyPerFileFlags(Processor const& processor,
        fs::path const& origin_file, std::list<std::string>& result_flags);
    static void executePlan(std::list<CompilationStep> const& steps,
            std::function<std::string(std::string const&)> flag_transform,
            IRecompilationChecker& deps_checker);
    static void gatherDirectories(std::list<Processor> const& processors,
            std::list<CompilationStep>& steps);
};

#endif /* TRAIN_COMPILATION_PACKAGEBUILDER_HPP */
