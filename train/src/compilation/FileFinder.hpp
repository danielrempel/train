#ifndef TRAIN_FILEFINDER_HPP
#define TRAIN_FILEFINDER_HPP

#include <regex>
#include <filesystem>
#include <list>

#include "../Visitor.hpp"

class FileFinder : private ConstVisitor<std::filesystem::directory_entry>
{
public:
    /* type string format: "\.*" (e.g. ".cpp") */
    FileFinder(std::filesystem::path const& source_folder,
            std::list<std::string> const& base_source_types,
            std::list<std::string> const& custom_source_types,
            std::list<std::filesystem::path>& binary_sources,
            std::list<std::filesystem::path>& other_sources);

    void Gather();

private:
    std::filesystem::path const& m_source_folder;
    std::list<std::filesystem::path>& m_binary_sources;
    std::list<std::filesystem::path>& m_other_sources;

    std::list<std::filesystem::path> m_possible_mains;
    std::list<std::regex> m_regexes;
    std::filesystem::path const m_bin_dir;

    void Visit(std::filesystem::directory_entry const& entry) override;

    static constexpr unsigned RECURSIVE_DESCENT_MAX_DEPTH = 16;
};

#endif /* TRAIN_FILEFINDER_HPP */
