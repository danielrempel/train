#ifndef TRAIN_COMPILATION_IRECOMPILATIONCHECKER_HPP
#define TRAIN_COMPILATION_IRECOMPILATIONCHECKER_HPP

class IRecompilationChecker;

#include "../entities/CompilationStep.hpp"

class IRecompilationChecker
{
public:
    IRecompilationChecker() = default;
    virtual ~IRecompilationChecker() noexcept = default;

    virtual bool shouldRecompile(CompilationStep const&) const = 0;
};

#endif /* TRAIN_COMPILATION_IRECOMPILATIONCHECKER_HPP */
