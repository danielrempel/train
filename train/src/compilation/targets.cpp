#include "targets.hpp"
#include <fmt/format.hpp>

void PrepareCompilationPaths(std::list<fs::path> const& sources,
        std::string const& dest_filetype,
        std::list<Processor> const& processors,
        std::map<std::string, std::list<Processor const*>>& out_paths);

/* ========================================================================= */
void TargetStaticLib(fs::path const& source_dir,
        fs::path const& build_dir, std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects)
{
    Processor const& ar = FindProcessor(processors,
            "AR");

    std::map<std::string, std::list<Processor const*>> compilation_paths;
    PrepareCompilationPaths(other_sources,
            ar.source_type(),
            processors,
            compilation_paths);

    std::list<fs::path> dummy_objects;
    CompileObjects(source_dir, build_dir,
            other_sources, compilation_paths,
            result_steps, dummy_objects);

    fs::path static_lib = build_dir / fmt::format("{}{}",
            package_name, ar.dest_type());

    result_objects.push_back(static_lib);

    result_steps.push_back(CompilationStep {
            dummy_objects,
            static_lib,
            &ar
        });
}

/* ========================================================================= */
void TargetAllBinaries(fs::path const& source_dir,
        fs::path const& build_dir, std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects)
{
    Processor const& linker = FindProcessor(processors,
            "LD");

    std::list<fs::path> dummy_objects;

    TargetStaticLib(source_dir, build_dir, other_sources, binary_sources,
            processors, package_name, result_steps, dummy_objects);
    fs::path static_lib = dummy_objects.back();

    std::map<std::string, std::list<Processor const*>> compilation_paths;
    PrepareCompilationPaths(binary_sources,
            linker.source_type(),
            processors,
            compilation_paths);

    std::list<fs::path> binaries_objects;
    CompileObjects(source_dir, build_dir,
            binary_sources, compilation_paths,
            result_steps, binaries_objects);

    for (auto const& object : binaries_objects)
    {
        fs::path binary = object;
        binary.replace_extension("");

        if (binary.filename().string() == "main")
        {
            binary.replace_filename(package_name);
        }

        result_steps.push_back(CompilationStep {
                { object, static_lib },
                binary,
                &linker
            });
    }
}

/* ========================================================================= */
void TargetSymlinkIncludeDir(fs::path const& abs_package_dir,
        fs::path const& rel_include_dir,
        fs::path const& abs_global_build_dir,
        std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects)
{
    Processor const& mkdir = FindProcessor(processors, "MKDIR");

    std::list<fs::path> empty_list;
    fs::path include_dir_base = abs_global_build_dir / rel_include_dir;

    result_steps.push_back(
        CompilationStep {
            empty_list,
            include_dir_base,
            &mkdir
        });

    Processor const& ln = FindProcessor(processors, "LN");

    std::list<fs::path> include_dir_src { abs_package_dir / rel_include_dir };
    fs::path include_dir_dest = include_dir_base / package_name;

    result_steps.push_back(
        CompilationStep {
            include_dir_src,
            include_dir_dest,
            &ln
        });
}

/* ========================================================================= */
void TargetSharedObjects(fs::path const& source_dir,
        fs::path const& build_dir, std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects)
{
    Processor const& compiler = FindProcessor(processors,
            "CXX-shared");

    std::map<std::string, std::list<Processor const*>> compilation_paths;
    PrepareCompilationPaths(other_sources,
            compiler.source_type(),
            processors,
            compilation_paths);

    CompileObjects(source_dir, build_dir,
            other_sources, compilation_paths,
            result_steps, result_objects);
}

/* ========================================================================= */
void TargetSharedLib(fs::path const& source_dir,
        fs::path const& build_dir, std::list<fs::path> const& other_sources,
        std::list<fs::path> const& binary_sources,
        std::list<Processor> const& processors,
        std::string const& package_name,
        std::list<CompilationStep>& result_steps,
        std::list<fs::path>& result_objects)
{
    Processor const& linker = FindProcessor(processors,
            "LD-shared");

    std::list<fs::path> intermediate_objects;

    TargetSharedObjects(source_dir, build_dir, other_sources, binary_sources,
            processors, package_name, result_steps, intermediate_objects);

    fs::path shared_lib = build_dir / fmt::format("{}{}",
            package_name, linker.dest_type());

    result_steps.push_back(
        CompilationStep {
            intermediate_objects,
            shared_lib,
            &linker
        });
}

/* ========================================================================= */
void PrepareCompilationPaths(std::list<fs::path> const& sources,
        std::string const& dest_filetype,
        std::list<Processor> const& processors,
        std::map<std::string, std::list<Processor const*>>& out_paths)
{
    std::list<std::string> filetypes;

    std::transform( sources.begin(),
                    sources.end(),
                    std::inserter(filetypes, filetypes.begin()),
                    [](fs::path const& path) -> std::string
                    {
                        return path.extension();
                    }
                );

    filetypes.sort();
    auto new_end = std::unique(filetypes.begin(), filetypes.end());
    filetypes.erase(new_end, filetypes.end());

    for (auto const& filetype : filetypes)
    {
        std::list<std::string> search_stack { filetype };

        std::list<Processor const*> compilation_path;
        RecursivelyFindCompilationPath(search_stack,
                compilation_path,
                processors,
                dest_filetype);

        out_paths[filetype] = compilation_path;
    }
}
