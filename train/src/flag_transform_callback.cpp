#include "flag_transform_callback.hpp"

#include <fmt/format.hpp>

std::string FlagTransformCallback(fs::path const& package_root,
        fs::path const& build_include_dir,
        fs::path const& package_build_dir,
        std::string const& flag)
{
    return (fmt::format(flag,
                fmt::arg("package_root", package_root.string()),
                fmt::arg("package_build_dir", package_build_dir.string()),
                fmt::arg("build_include_dir", build_include_dir.string())
            ));
}
