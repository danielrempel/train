#include <fmt/format.hpp>
#include <fmt/ostream.hpp>

#include "common_header.hpp"

#include "strings.hpp"
#include "entities/Workspace.hpp"
#include "entities/Package.hpp"

#include "compilation/PackageBuilder.hpp"

#include "TaskQueue.hpp"

struct Node
{
    std::string m_value;
    std::list<Node> m_children;
};

// As a lazy substitution for dependency loop detection
constexpr size_t MAX_DEPENDENCY_DEPTH = 16;
/* ========================================================================= */
static void GatherBuildQueue(Workspace const& ws, Package const& pkg,
        std::list<std::string>& dest_buildqueue, size_t depth);

/* ========================================================================= */
int main(int argc, char const** argv)
{
    TaskQueue task_queue;

    // Initialize workspace in current dir
    Workspace ws(fs::current_path(), task_queue);

    // Initialize package in current dir
    Package& target_pkg = ws.loadChildPackage(fs::current_path());

    std::list<std::string> build_queue { target_pkg.name() };

    task_queue.runAll();

    GatherBuildQueue(ws, target_pkg, build_queue, 0);

    // Build everything but the target package
    build_queue.pop_back();
    std::list<std::string> args;

    for (auto const& pkg_name : build_queue)
    {
        Package& pkg_to_build = ws.getPackage(pkg_name);

		// Collect link flags from dependencies for the target pkg
		// TODO: maybe generateLinkFlags() should return a list, too?
		args.push_back(pkg_to_build.generateLinkFlags());
		fmt::print("A library to link: {}\n",
					pkg_to_build.generateLinkFlags());

        PackageBuilder builder(ws, pkg_to_build);
        builder.build();
    }

    PackageBuilder final_builder(ws, target_pkg);
    final_builder.setBinaryPkgLinkDeps(std::move(args));
    final_builder.build();

    return (0);
}

/* ========================================================================= */
void GatherBuildQueue(Workspace const& ws, Package const& pkg,
        std::list<std::string>& dest_buildqueue, size_t depth)
{
    if (MAX_DEPENDENCY_DEPTH == depth)
    {
        fmt::print("GatherBuildQueue(): dependency tree too deep\n");
        throw std::runtime_error("GatherBuildQueue(): dependency tree too deep");
    }

    auto const& deps = pkg.dependencies();

    for (auto const& dep : deps)
    {
        /* If we spot the same package twice in the tree, move it forward in
         * the build queue */
        do
        {
            auto iter = std::find(dest_buildqueue.begin(), dest_buildqueue.end(),
                    dep);
            if (dest_buildqueue.end() != iter)
            {
                dest_buildqueue.erase(iter);
            } else {
                break;
            }
        } while (true);

        dest_buildqueue.push_front(dep);

        auto const& dep_pkg = ws.getPackage(dep);
        GatherBuildQueue(ws, dep_pkg, dest_buildqueue, depth + 1);
    }
}
