#ifndef TRAIN_VISITOR_HPP
#define TRAIN_VISITOR_HPP

template<typename T>
class Visitor
{
public:
    virtual ~Visitor<T>() noexcept
    {}

    virtual void Visit(T& object) = 0;
};

template<typename T>
class ConstVisitor
{
public:
    virtual ~ConstVisitor<T>() noexcept
    {}

    virtual void Visit(T const& object) = 0;
};

#endif /* TRAIN_VISITOR_HPP */
