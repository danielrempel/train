#ifndef TRAIN_COMMON_HEADER_HPP
#define TRAIN_COMMON_HEADER_HPP

#include <memory>
#include <filesystem>

namespace fs = std::filesystem;

using std::unique_ptr;
using std::shared_ptr;
using std::make_unique;
using std::make_shared;

#endif /* TRAIN_COMMON_HEADER_HPP */
