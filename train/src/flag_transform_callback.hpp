#ifndef TRAIN_FLAG_TRANSFORM_CBACK_HPP
#define TRAIN_FLAG_TRANSFORM_CBACK_HPP

#include <string>
#include "common_header.hpp"

/* Transform compiler flags using fmt::format
 * Currently supports:
 *  * {package_root:}
 *  * {package_build_dir:}
 *  * {build_include_dir:}
 */
std::string FlagTransformCallback(fs::path const& package_root,
        fs::path const& build_include_dir,
        fs::path const& package_build_dir,
        std::string const& flag);

#endif /* TRAIN_FLAG_TRANSFORM_CBACK_HPP */
