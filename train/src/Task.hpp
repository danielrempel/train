#ifndef TRAIN_TASK_HPP
#define TRAIN_TASK_HPP

class Task;

class Task
{
public:
    virtual ~Task() noexcept
    {};

    virtual void run() = 0;
};

#endif /* TRAIN_TASK_HPP */
