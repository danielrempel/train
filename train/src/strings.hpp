#ifndef TRAIN_STRINGS_HPP
#define TRAIN_STRINGS_HPP

#define STRINGS_APP_NAME "Train"
#define STRINGS_CONFIG_FIlE_NAME "config.toml"
#define STRINGS_MANIFEST_FILE_NAME "train.toml"

#define STRINGS_SOURCE_DIR "src"
#define STRINGS_INCLUDE_DIR "include"
#define STRINGS_BUILD_DIR "build"

#define STRINGS_TOML_PROCESSOR_SECTION "processor"
#define STRINGS_TOML_FILE_SECTION "file"

// TODO: add all the toml field and section names used by Processor, Workspace

#endif /* TRAIN_STRINGS_HPP */
