#include "Processor.hpp"

#include <fmt/format.hpp>

Processor::Processor(std::string const& name, std::string const& source_type,
        std::string const& dest_type, std::string const& format_string,
        std::string const& command, std::list<std::string>&& flags)
    : m_name(name), m_source_type(source_type), m_dest_type(dest_type),
        m_format_string(format_string), m_command(command),
        m_flags(std::move(flags))
{}

Processor::Processor(Processor const& other)
    : m_name(other.m_name), m_source_type(other.m_source_type),
        m_dest_type(other.m_dest_type), m_format_string(other.m_format_string),
        m_command(other.m_command), m_flags(other.m_flags),
        m_additions(other.m_additions)
{}

Processor::~Processor()
{}

Processor& Processor::operator=(Processor const& other)
{
    if (&other != this)
    {
        m_name = other.m_name;
        m_source_type = other.m_source_type;
        m_dest_type = other.m_dest_type;
        m_format_string = other.m_format_string;
        m_command = other.m_command;
        m_flags = other.m_flags;
        m_additions = other.m_additions;
    }
    return (*this);
}

Processor Processor::MergeProcessors(Processor const& base,
        Processor const& extension)
{
    auto name = base.m_name;

    auto source_type = base.m_source_type;
    if ("" != extension.m_source_type)
    {
        source_type = extension.m_source_type;
    }

    auto dest_type = base.m_dest_type;
    if ("" != extension.m_dest_type)
    {
        dest_type = extension.m_dest_type;
    }

    auto format_string = base.m_format_string;
    if ("" != extension.m_format_string)
    {
        format_string = extension.m_format_string;
    }

    auto command = base.m_command;
    if ("" != extension.m_command)
    {
        command = extension.m_command;
    }

    auto flags = base.m_flags;
    auto extension_flags = extension.m_flags;
    flags.splice(flags.end(), extension_flags);

    auto additions = base.m_additions;
    auto extension_additions = extension.m_additions;
    additions.merge(extension_additions);

    Processor retval(name, source_type, dest_type, format_string, command,
            std::move(flags));
    retval.m_additions = std::move(additions);

    return (retval);
}

bool Processor::isValid() const
{
    return ("" != m_name and
            "" != m_format_string and
            "" != m_command);
}

Processor::Additions& Processor::additions(fs::path const& rel_origin)
{
    return (m_additions[rel_origin]);
}

Processor::Additions const& Processor::additions(fs::path const& rel_origin) const
{
    return (m_additions.at(rel_origin));
}

void Processor::appendFlags(std::list<std::string>&& flags)
{
    m_flags.splice(m_flags.end(), std::move(flags));
}
