#ifndef TRAIN_WORKSPACE_HPP
#define TRAIN_WORKSPACE_HPP

#include <list>
#include <map>
#include "../common_header.hpp"

#include <toml11/toml11.hpp>

class Workspace;

#include "Processor.hpp"
#include "Package.hpp"
#include "../TaskQueue.hpp"

/* ========================================================================= */
class Workspace final
{
public:
    Workspace(fs::path const& abs_workspace_location, TaskQueue& task_queue);

    Workspace(Workspace const&) = delete;
    Workspace(Workspace&&) = delete;

    Workspace& operator=(Workspace const&) = delete;
    Workspace& operator=(Workspace&&) = delete;

    /** Load Workspace's child package **/
    Package& loadChildPackage(fs::path const& package_location);

    /** Queue loading of a dependency package
     *
     * Queues dependency load in the m_task_queue. The queued task is to call
     * Workspace::actuallyLoadDependency()
     **/
    void queueDependencyLoad(std::string const& name, toml::table const& def,
            fs::path const& abs_base_dir);
    /** Queue loading of a dependency package **/
    void queueDependencyLoad(std::string const& name, std::string const& version);

    Package& getPackage(std::string const& name);
    Package const& getPackage(std::string const& name) const;

    fs::path const& rel_source_dir() const;
    fs::path const& rel_include_dir() const;
    fs::path const& abs_build_base_dir() const;
    fs::path const& abs_build_include_dir() const;

    std::list<Processor> const& base_processors() const;
    std::list<std::string> const& base_source_types() const;

    fs::path getBuildPath(std::string const& package_name) const;
private:
    fs::path const m_abs_base_dir;

    fs::path const m_rel_source_dir;
    fs::path const m_rel_include_dir;
    fs::path const m_rel_build_dir;
    fs::path const m_abs_build_base_dir;
    fs::path const m_abs_build_include_dir;

    toml::value const m_global_config_toml;

    std::list<Processor> m_base_processors;
    std::list<std::string> m_base_source_types;

    std::list<std::string> m_child_packages;
    std::map<std::string, Package> m_packages;

    TaskQueue& m_task_queue;

    /** Load Processors definitions from the global config. Quite naiive **/
    static void loadProcessors(toml::array const& processors_cfg,
            std::list<Processor>& processors,
            std::list<std::string>& source_types);

    friend class LocalDependencyLoadTask;
    void actuallyLoadDependency(std::string const& name,
            fs::path const& package_location);
};

/* ========================================================================= */
inline fs::path const& Workspace::rel_source_dir() const
{
    return (m_rel_source_dir);
}

/* ========================================================================= */
inline fs::path const& Workspace::rel_include_dir() const
{
    return (m_rel_include_dir);
}

/* ========================================================================= */
inline fs::path const& Workspace::abs_build_base_dir() const
{
    return (m_abs_build_base_dir);
}

/* ========================================================================= */
inline fs::path const& Workspace::abs_build_include_dir() const
{
    return (m_abs_build_include_dir);
}

/* ========================================================================= */
inline std::list<Processor> const& Workspace::base_processors() const
{
    return (m_base_processors);
}

/* ========================================================================= */
inline std::list<std::string> const& Workspace::base_source_types() const
{
    return (m_base_source_types);
}

/* ========================================================================= */
inline Package& Workspace::getPackage(std::string const& package_name)
{
    return (m_packages.at(package_name));
}

/* ========================================================================= */
inline Package const& Workspace::getPackage(std::string const& package_name) const
{
    return (m_packages.at(package_name));
}

#endif /* TRAIN_WORKSPACE_HPP */
