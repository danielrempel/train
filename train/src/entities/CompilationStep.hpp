#ifndef TRAIN_COMPILATION_COMPILATIONPATH_HPP
#define TRAIN_COMPILATION_COMPILATIONPATH_HPP

#include <list>

#include "../common_header.hpp"

struct CompilationStep;

#include "../entities/Processor.hpp"

struct CompilationStep
{
    std::list<fs::path> sources;
    fs::path destination;
    Processor const* processor;

    // Where applicable
    // Used to apply per-file compile flags
    fs::path origin_file;
};

#endif /* TRAIN_COMPILATION_COMPILATIONPATH_HPP */
