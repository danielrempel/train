#ifndef TRAIN_PROCESSOR_HPP
#define TRAIN_PROCESSOR_HPP

#include <string>
#include <list>
#include <map>
#include "../common_header.hpp"

class Processor;

/* ========================================================================= */
class Processor final
{
public:
    Processor(std::string const& name, std::string const& source_type,
            std::string const& dest_type, std::string const& format_string,
            std::string const& command, std::list<std::string>&& flags);
    Processor(Processor const& other);

    ~Processor();

    Processor(Processor&&) = delete;

    Processor& operator=(Processor const& other);
    Processor& operator=(Processor&&) = delete;

    /* Merges two instances of the same processor. The name is assumed to be
     * equal and is taken from <base>
     * Take all the string values from the <base>,
     * replace them with values from <extension> where they are not ""
     *
     * m_flags are concatenated
     * m_additions are joined in lazy ways: all keys not existing in base
     *     are added to it
     */
    static Processor MergeProcessors(Processor const& base,
            Processor const& extension);

    bool isValid() const;
    std::string const& name() const;

    std::string const& source_type() const;
    std::string const& dest_type() const;
    std::string const& format_string() const;
    std::string const& command() const;
    std::list<std::string> const& flags() const;

    void appendFlags(std::list<std::string>&& flags);

    struct Additions;
    bool hasAddition(fs::path const& rel_origin) const;
    Additions& additions(fs::path const& rel_origin);
    Additions const& additions(fs::path const& rel_origin) const;

    std::string generateCommandLine(std::list<fs::path> rel_sources,
            fs::path const& rel_dest, fs::path const& rel_origin);
private:
    std::string m_name;
    std::string m_source_type;
    std::string m_dest_type;
    std::string m_format_string;
    std::string m_command;

    std::list<std::string> m_flags;

    std::map<fs::path, Additions> m_additions;
};

/* ========================================================================= */
struct Processor::Additions
{
    std::list<std::string> add_flags;
    std::list<std::string> remove_flags;
};

/* ========================================================================= */
inline std::string const& Processor::name() const
{
    return (m_name);
}

/* ========================================================================= */
inline std::string const& Processor::source_type() const
{
    return (m_source_type);
}

/* ========================================================================= */
inline std::string const& Processor::dest_type() const
{
    return (m_dest_type);
}

/* ========================================================================= */
inline std::string const& Processor::format_string() const
{
    return (m_format_string);
}

/* ========================================================================= */
inline std::string const& Processor::command() const
{
    return (m_command);
}

/* ========================================================================= */
inline std::list<std::string> const& Processor::flags() const
{
    return (m_flags);
}

/* ========================================================================= */
inline bool Processor::hasAddition(fs::path const& rel_origin) const
{
    return (m_additions.count(rel_origin) > 0);
}

#endif /* TRAIN_PROCESSOR_HPP */
