#include "Package.hpp"

#include <fmt/format.hpp>

#include "../strings.hpp"
#include "../flag_transform_callback.hpp"
#include "../toml_util.hpp"
#include "../compilation/FileFinder.hpp"

/* ========================================================================= */
Package::Package(fs::path const& abs_package_location, Workspace& workspace)
    : m_abs_package_dir(abs_package_location),
      m_abs_source_dir((m_abs_package_dir / workspace.rel_source_dir())
              .lexically_normal()),
      m_manifest_toml(toml::parse(
                m_abs_package_dir / STRINGS_MANIFEST_FILE_NAME )),
      m_base_processors(workspace.base_processors()),
      m_base_source_types(workspace.base_source_types()),
      m_gathered_sources(false),
      m_type(Type::Empty)
{
    std::string name = m_manifest_toml
        .at("package").as_table()
        .at("name").as_string();
    m_name = name;

    m_abs_build_dir = workspace.getBuildPath(m_name);

    fmt::print("Package: '{}'\n", name);

    std::string package_type_string = m_manifest_toml
        .at("package").as_table()
        .at("type").as_string();
    if ("bin" == package_type_string)
    {
        m_type = Type::Bin;
    } else if ("lib" == package_type_string)
    {
        m_type = Type::Lib;
    } else if ("so" == package_type_string)
    {
        m_type = Type::SO;
    } else {
        fmt::print("{}: {:03d}: unknown package.type '{}'\n", __FILE__,
                __LINE__, package_type_string);
        throw std::runtime_error("Unknown package.type");
    }

    // Load Processors - Package's version
    if (m_manifest_toml.count(STRINGS_TOML_PROCESSOR_SECTION))
    {
        auto const& processors_toml = toml::find<toml::array>(m_manifest_toml,
                STRINGS_TOML_PROCESSOR_SECTION);
        loadProcessors(processors_toml, m_processors, m_base_processors,
                m_source_types, m_base_source_types);
    }

    // Per file flags
    if (m_manifest_toml.count(STRINGS_TOML_FILE_SECTION))
    {
        auto const& files_toml = toml::find<toml::array>(m_manifest_toml,
                STRINGS_TOML_FILE_SECTION);
        loadPerFileFlags(files_toml, m_processors, m_base_processors);
    }

    // Dependencies
    if (m_manifest_toml.count("dependencies"))
    {
        auto const& deps_toml = toml::find<toml::table>(m_manifest_toml,
                "dependencies");
        loadDependencies(m_dependencies, deps_toml, workspace, m_abs_package_dir);
    }
}

/* ========================================================================= */
void Package::gatherSourceFiles()
{
    if (m_gathered_sources)
    {
        return;
    }
    /* Gathers source files in the form of paths relative to source_dir */
    FileFinder finder(m_abs_source_dir, m_base_source_types, m_source_types,
            m_binary_sources, m_other_sources);
    finder.Gather();
    m_gathered_sources = true;
}

/* ========================================================================= */
/* static */ void Package::loadProcessors(toml::array const& processors_cfg,
        std::list<Processor>& processors,
        std::list<Processor> const& base_processors,
        std::list<std::string>& source_types,
        std::list<std::string> const& base_source_types)
{
    for (unsigned i = 0; i < processors_cfg.size(); ++i)
    {
        auto const& proc = toml::get<toml::table>(processors_cfg.at(i));
        std::string name = toml::get<toml::string>(proc.at("name"));

        /* fmt::print("Package::loadProcessors(): name = {}\n", name); */

        std::string input_type;
        if (proc.end() != proc.find("input_type"))
        {
            input_type = toml::get<toml::string>(proc.at("input_type"));
        }

        std::string output_type;
        if (proc.end() != proc.find("output_type"))
        {
            output_type = toml::get<toml::string>(proc.at("output_type"));
        }

        std::string format_string;
        if (proc.end() != proc.find("format_string"))
        {
            format_string = toml::get<toml::string>(proc.at("format_string"));
        }

        std::string command;
        if (proc.end() != proc.find("command"))
        {
            command = toml::get<toml::string>(proc.at("command"));
        }

        std::list<std::string> flags;
        if (proc.end() != proc.find("flags"))
        {
            TomlArrayToStringList(toml::get<toml::array>(proc.at("flags")),
                    flags);
        }

        /* fmt::print("Package::loadProcessors(): Processor {{ {}, {} -> {}, "
                "\"{}\", {}, ... }}\n", name, input_type, output_type,
                format_string, command); */

        Processor ext_proc(name, input_type, output_type, format_string, command,
                std::move(flags));

        /* Merge with base processor */
        auto iter_base_processor = std::find_if(base_processors.begin(),
                base_processors.end(),
                [&](Processor const& p) -> bool
                {
                    return (p.name() == name);
                });
        if (base_processors.end() != iter_base_processor)
        {
            Processor const& base = *iter_base_processor;

            Processor merged = Processor::MergeProcessors(base, ext_proc);
            ext_proc = merged;
        }

        // assert that the processor is valid
        if (not ext_proc.isValid())
        {
            fmt::print("{}: {:03d}: incomplete processor '{}'\n",
                    __FILE__, __LINE__, ext_proc.name());
            throw std::runtime_error("Incomplete processor!");
        }

        // At this point we're sure that we've got a full processor that can be
        // used
        processors.push_back(ext_proc);

        // We may need to add the input type to the list of "source types"
        if (proc.end() != proc.find("input_is_source") and
                proc.at("input_is_source").as_boolean())
        {
            auto iter_base_source_type = std::find_if(base_source_types.begin(),
                    base_source_types.end(),
                    [&](std::string const& s) -> bool
                    {
                        return (s == input_type);
                    });

            if (base_source_types.end() == iter_base_source_type)
            {
                source_types.push_back(input_type);
            }
        }
    }
}

/* ========================================================================= */
/* static */ void Package::loadPerFileFlags(toml::array const& files_cfg,
        std::list<Processor>& processors,
        std::list<Processor> const& base_processors)
{
    for (unsigned i = 0; i < files_cfg.size(); ++i)
    {
    try
    {
        auto const& file_entry = toml::get<toml::table>(files_cfg.at(i));

        std::string origin = file_entry.at("file").as_string();
        fs::path origin_path(origin);

        std::string processor = file_entry.at("processor").as_string();

        // Look for a custom processor
        auto find_processor =  [&](Processor const& p) -> bool
                {
                    return (p.name() == processor);
                };
        auto iter_custom_processor = std::find_if(processors.begin(),
                processors.end(), find_processor);

        // If didn't find a custom one, look for base one
        if (processors.end() == iter_custom_processor)
        {
            auto iter_base_processor = std::find_if(base_processors.begin(),
                    base_processors.end(), find_processor);

            // O-o-opsie
            if (base_processors.end() == iter_base_processor)
            {
                fmt::print("Package::loadPerFileFlags(): processor {} for "
                        "file {} not found!\n", processor, origin);

                throw std::runtime_error("Package::loadPerFileFlags(): "
                        "processor not found!");
            }

            // Copy the base processor into the custom ones list
            processors.emplace_back(*iter_base_processor);
            iter_custom_processor = processors.end();
            --iter_custom_processor;
        }

        Processor& the_processor = *iter_custom_processor;
        Processor::Additions& additions = the_processor.additions(origin_path);

        if (file_entry.count("add_flags"))
        {
            TomlArrayToStringList(file_entry.at("add_flags").as_array(),
                    additions.add_flags);
        }

        if (file_entry.count("remove_flags"))
        {
            TomlArrayToStringList(file_entry.at("remove_flags").as_array(),
                    additions.remove_flags);
        }
    }
    catch (std::out_of_range const& oor)
    {
        fmt::print("Package::loadPerFileFlags(): incomplete entry; {}\n",
                oor.what());

        throw std::runtime_error("Package : per file flags entry is wrong");
    }
    }
}

/* ========================================================================= */
/* static */ void Package::loadDependencies(std::list<std::string>& dependencies,
        toml::table const& deps_cfg, Workspace& workspace,
        fs::path const& abs_base_dir)
{
    for (auto const& [key, value] : deps_cfg)
    {
        fmt::print("dependency name: '{}'\n", key);

        if (value.is_string())
        {
            workspace.queueDependencyLoad(key, value.as_string());
        } else if (value.is_table())
        {
            workspace.queueDependencyLoad(key, value.as_table(), abs_base_dir);
        } else
        {
            fmt::print("Package::loadDependencies(): wrong dependency "
                    "definition: {}\n", key);
            throw std::runtime_error("Package::loadDependencies(): wrong "
                    "dependency defintion");
        }

        dependencies.push_back(key);
    }
}

/* ========================================================================= */
std::string Package::generateLinkFlags() const
{
    if (type() == Package::Type::Bin)
    {
        throw std::runtime_error("No link flags from a binary");
    }

    std::string file_ext;

    if (type() == Package::Type::Lib)
    {
        // TODO: replace the hardcoded value
        file_ext = "a";
    }

    if (type() == Package::Type::SO)
    {
        // TODO: replace the hardcoded value
        file_ext = "so";
    }

    std::string result = fmt::format("{}/{}.{}", abs_build_dir().string(),
            name(), file_ext);

    return (result);
}
