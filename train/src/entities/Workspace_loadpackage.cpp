#include "Workspace.hpp"

#include <fmt/format.hpp>

/* ========================================================================= */
class LocalDependencyLoadTask : public Task
{
public:
    LocalDependencyLoadTask(Workspace& workspace, std::string package_name,
            fs::path path);
    ~LocalDependencyLoadTask() noexcept
    {}

    void run() override;
private:
    Workspace& m_workspace;

    std::string m_package_name;
    fs::path m_package_path;
};

/* ========================================================================= */
LocalDependencyLoadTask::LocalDependencyLoadTask(Workspace& workspace,
        std::string package_name, fs::path path)
    : m_workspace(workspace), m_package_name(package_name), m_package_path(path)
{}

/* ========================================================================= */
void LocalDependencyLoadTask::run()
{
    m_workspace.actuallyLoadDependency(m_package_name, m_package_path);
}

/* ========================================================================= */
void Workspace::queueDependencyLoad(std::string const& name,
        toml::table const& def, fs::path const& abs_base_dir)
{
    if (def.count("path") > 0)
    {
        std::string path_value = def.at("path").as_string();
        fs::path dependency_path = abs_base_dir / fs::path(path_value);
        dependency_path = dependency_path.lexically_normal();

        m_task_queue.pushTask(make_unique<LocalDependencyLoadTask>(*this, name,
                dependency_path));
    } else {
        fmt::print("Workspace::queueDependencyLoad(version): not implemented\n");
        throw std::runtime_error("not implemented");
    }
}

/* ========================================================================= */
void Workspace::queueDependencyLoad(std::string const& name,
        std::string const& version)
{
    fmt::print("Workspace::queueDependencyLoad(version): not implemented\n");
    throw std::runtime_error("not implemented");
}
