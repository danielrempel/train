#include "Workspace.hpp"

#include <fmt/format.hpp>
#ifndef DEBUG
#include <PlatformFolders/platform_folders.hpp>
#endif /* DEBUG */

#include "../strings.hpp"

/* ========================================================================= */
static std::string GetGlobalConfigFilename()
{
#ifdef DEBUG
    return std::string("./global_") + STRINGS_CONFIG_FIlE_NAME;
#else
    return PlatformFolders::getConfigHome() + "/" + STRINGS_APP_NAME + "/" + STRINGS_CONFIG_FIlE_NAME;
#endif /* DEBUG */
}

/* ========================================================================= */
Workspace::Workspace(fs::path const& workspace_location, TaskQueue& task_queue)
    : m_abs_base_dir(workspace_location.lexically_normal()),
        m_rel_source_dir(STRINGS_SOURCE_DIR),
        m_rel_include_dir(STRINGS_INCLUDE_DIR),
        m_rel_build_dir(STRINGS_BUILD_DIR),
        m_abs_build_base_dir(m_abs_base_dir / STRINGS_BUILD_DIR),
        m_abs_build_include_dir(m_abs_build_base_dir / STRINGS_INCLUDE_DIR),
        m_global_config_toml(toml::parse(GetGlobalConfigFilename())),
        m_task_queue(task_queue)
{
    fmt::print("Workspace: global config file: '{}'\n",
            GetGlobalConfigFilename());

    /*fmt::print("Workspace: loading processors\n");*/
    if (m_global_config_toml.count(STRINGS_TOML_PROCESSOR_SECTION))
    {
        auto const& processors_cfg = toml::find<toml::array>(m_global_config_toml,
            STRINGS_TOML_PROCESSOR_SECTION);

        Workspace::loadProcessors(processors_cfg, m_base_processors,
                m_base_source_types);
    }
}

/* ========================================================================= */
Package& Workspace::loadChildPackage(fs::path const& package_location)
{
    fs::path location = package_location.lexically_normal();

    Package package(location, *this);

    std::string package_name = package.name();
    auto [iter, is_inserted] = m_packages.emplace(package_name, std::move(package));

    /* iterator<std::pair<key,value>> */
    Package& pkg = iter->second;

    m_child_packages.push_back(package_name);

    return (pkg);
}

/* ========================================================================= */
void Workspace::actuallyLoadDependency(std::string const& package_name,
            fs::path const& package_location)
{
    fs::path location = package_location.lexically_normal();

    Package package(location, *this);
    auto [iter, is_inserted] = m_packages.emplace(package_name, std::move(package));

    /* iterator<std::pair<key,value>> */
    Package& pkg = iter->second;

    if (not (Package::Type::Lib == pkg.type() or
            Package::Type::SO == pkg.type()))
    {
        fmt::print("Workspace::actuallyLoadDependency(): package {} is not a "
                "library\n", package_name);
        throw std::runtime_error("package depending on a binary");
    }
}

/* ========================================================================= */
fs::path Workspace::getBuildPath(std::string const& package_name) const
{
    return (m_abs_build_base_dir / package_name).lexically_normal();
}
