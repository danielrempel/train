#include "Workspace.hpp"

#include <fmt/format.hpp>

#include "../toml_util.hpp"

/* ========================================================================= */
void Workspace::loadProcessors(toml::array const& processors_cfg,
        std::list<Processor>& processors,
        std::list<std::string>& source_types)
{
    for (unsigned i = 0; i < processors_cfg.size(); ++i)
    {
        std::string name;
    try
    {
        auto const& proc = toml::get<toml::table>(processors_cfg.at(i));

        name = toml::get<toml::string>(proc.at("name"));
        std::string source_type = toml::get<toml::string>(proc.at("input_type"));
        std::string dest_type = toml::get<toml::string>(proc.at("output_type"));
        std::string format_string = toml::get<toml::string>(proc.at("format_string"));
        std::string command = toml::get<toml::string>(proc.at("command"));

        std::list<std::string> flags;
        if (proc.count("flags"))
        {
            TomlArrayToStringList(toml::get<toml::array>(proc.at("flags")), flags);
        }

        processors.emplace_back(name, source_type, dest_type, format_string,
                command, std::move(flags));

        /* fmt::print("Workspace: processor '{}'\n", name); */

        if (proc.end() != proc.find("input_is_source") and
                toml::get<toml::boolean>(proc.at("input_is_source")))
        {
            auto const& type = source_type;

            if (source_types.end() == std::find(source_types.begin(),
                        source_types.end(), type))
            {
                source_types.push_back(type);
            }
        }
    }
    catch (std::out_of_range const& oor)
    {
        fmt::print("Workspace::loadProcessors(): one of the processors "
                "configuration fields not found\n");
        if ("" != name)
        {
            fmt::print("Workspace::loadProcessors(): erroneous processor: {}\n",
                    name);
        }

        throw std::runtime_error("Incomplete processor definition in the "
                "global configuration");
    }
    }
}
