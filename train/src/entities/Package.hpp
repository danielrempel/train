#ifndef TRAIN_PACKAGE_HPP
#define TRAIN_PACKAGE_HPP

#include <string>
#include <list>
#include "../common_header.hpp"

#include <toml11/toml11.hpp>

class Package;

#include "Workspace.hpp"
#include "Processor.hpp"

/* ========================================================================= */
/** A representation of a physically present package
 *
 * Represents a package - its path, configuration, sources lists, type,
 * dependencies
 */
class Package final
{
public:
    enum class Type;

    Package(fs::path const& abs_package_location, Workspace& workspace);

    std::string const& name() const;
    Type type() const;

    fs::path const& abs_build_dir() const;
    fs::path const& abs_package_dir() const;
    fs::path const& abs_source_dir() const;

    /* Generate link flags for users of this package */
    std::string generateLinkFlags() const;

    void gatherSourceFiles();

    std::list<Processor> const& custom_processors() const;
    std::list<fs::path> const& other_sources() const;
    std::list<fs::path> const& binary_sources() const;

    std::list<std::string> const& dependencies() const;

    enum class Type
    {
        Empty,
        Bin,
        Lib,
        SO
    };
private:
    fs::path const m_abs_package_dir;
    fs::path const m_abs_source_dir;

    fs::path m_abs_build_dir;

    toml::value const m_manifest_toml;

    std::list<Processor> const& m_base_processors;
    std::list<Processor> m_processors;

    // loadProcessors() ensures there are no duplicates!
    std::list<std::string> const& m_base_source_types;
    std::list<std::string> m_source_types;

    std::list<std::string> m_dependencies;

    bool m_gathered_sources;
    std::list<fs::path> m_binary_sources;
    std::list<fs::path> m_other_sources;

    std::string m_name;
    Type m_type;

    /** Load Processors and their extensions from the package manifest
     *
     * Contrary to the Workspace's method, this one treats processor
     * definitions as extensions: fields may be empty and in such case it is
     * supposed that there's a "base processor" with the same name
     */
    static void loadProcessors(toml::array const& processors_cfg,
            std::list<Processor>& processors,
            std::list<Processor> const& base_processors,
            std::list<std::string>& source_types,
            std::list<std::string> const& base_source_types);

    /** Load per file flags from the package manifest
     */
    static void loadPerFileFlags(toml::array const& files_cfg,
        std::list<Processor>& processors,
        std::list<Processor> const& base_processors);

    static void loadDependencies(std::list<std::string>& dependencies,
        toml::table const& deps_cfg, Workspace& workspace,
        fs::path const& abs_base_dir);
};

/* ========================================================================= */
inline std::string const& Package::name() const
{
    return (m_name);
}

/* ========================================================================= */
inline Package::Type Package::type() const
{
    return (m_type);
}

/* ========================================================================= */
inline std::list<Processor> const& Package::custom_processors() const
{
    return (m_processors);
}

/* ========================================================================= */
inline std::list<fs::path> const& Package::other_sources() const
{
    return (m_other_sources);
}

/* ========================================================================= */
inline std::list<fs::path> const& Package::binary_sources() const
{
    return (m_binary_sources);
}

/* ========================================================================= */
inline fs::path const& Package::abs_build_dir() const
{
    return (m_abs_build_dir);
}

/* ========================================================================= */
inline fs::path const& Package::abs_package_dir() const
{
    return (m_abs_package_dir);
}

/* ========================================================================= */
inline fs::path const& Package::abs_source_dir() const
{
    return (m_abs_source_dir);
}

/* ========================================================================= */
inline std::list<std::string> const& Package::dependencies() const
{
    return (m_dependencies);
}

#endif /* TRAIN_PACKAGE_HPP */
