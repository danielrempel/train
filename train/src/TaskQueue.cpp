#include "TaskQueue.hpp"

TaskQueue::TaskQueue()
{}

TaskQueue::~TaskQueue() noexcept
{}

void TaskQueue::pushTask(unique_ptr<Task>&& task)
{
    m_queue.push_back(std::move(task));
}

void TaskQueue::step()
{
    if (hasTasks())
    {
        auto task = std::move(m_queue.front());
        m_queue.pop_front();

        task->run();
    }
}

bool TaskQueue::hasTasks() const
{
    return (not m_queue.empty());
}

void TaskQueue::runAll()
{
    while (hasTasks())
    {
        auto task = std::move(m_queue.front());
        m_queue.pop_front();

        task->run();
    }
}
