#ifndef TRAIN_TOML_UTIL_HPP
#define TRAIN_TOML_UTIL_HPP

#include <string>
#include <list>

#include <toml11/toml11.hpp>

void TomlArrayToStringList(toml::array const& array,
        std::list<std::string>& list);

#endif /* TRAIN_TOML_UTIL_HPP */
