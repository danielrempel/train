#include "toml_util.hpp"

/* ========================================================================= */
void TomlArrayToStringList(toml::array const& array,
        std::list<std::string>& list)
{
    for (unsigned long i = 0; i < array.size(); ++i)
    {
        list.push_back(
            toml::get<toml::string>(array.at(i))
                );
    }
}
