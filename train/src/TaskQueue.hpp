#ifndef TRAIN_TASKQUEUE_HPP
#define TRAIN_TASKQUEUE_HPP

#include <list>
#include "common_header.hpp"

class TaskQueue;

#include "Task.hpp"

class TaskQueue final
{
public:
    TaskQueue();
    ~TaskQueue() noexcept;

    void pushTask(unique_ptr<Task>&& task);

    void step();
    bool hasTasks() const;

    void runAll();

private:
    std::list<unique_ptr<Task>> m_queue;
};

#endif /* TRAIN_TASKQUEUE_HPP */
