/*
Its is under the MIT license, to encourage reuse by cut-and-paste.

The original files are hosted here: https://github.com/PlatformFolders007/PlatformFolders

Copyright (c) 2015 Poul Sander

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef PFOLDERS_CLASS_HPP
#define PFOLDERS_CLASS_HPP

#include <vector>
#include <string>

namespace PlatformFolders
{

#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace internal {
#if !defined(_WIN32) && !defined(__APPLE__)
void appendExtraFoldersTokenizer(const char* envName, const char* envValue, std::vector<std::string>& folders);
#endif
#ifdef _WIN32
std::string win32_utf16_to_utf8(const wchar_t* wstr);
#endif
}
#endif  //DOXYGEN_SHOULD_SKIP_THIS

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/**
 * This class contains methods for finding the system depended special folders.
 * For Windows these folders are either by convention or given by CSIDL.
 * For Linux XDG convention is used.
 * The Linux version has very little error checking and assumes that the config is correct
 */
class PlatformFolders {
public:
	PlatformFolders();
	~PlatformFolders();
	/**
	 * The folder that represents the desktop.
	 * Normally you should try not to use this folder.
	 * @return Absolute path to the user's desktop
	 */
	std::string getDesktopFolder() const;
	/**
	 * The folder to store user documents to
	 * @return Absolute path to the "Documents" folder
	 */
	std::string getDocumentsFolder() const;
	/**
	 * The folder for storing the user's pictures.
	 * @return Absolute path to the "Picture" folder
	 */
	std::string getPicturesFolder() const;
	/**
	 * Use PlatformFolders::getPublicFolder() instead!
	 */
	std::string getPublicFolder() const;
	/**
	 * The folder where files are downloaded.
	 * @note Windows: This version is XP compatible and returns the Desktop. Vista and later has a dedicated folder.
	 * @return Absolute path to the folder where files are downloaded to.
	 */
	std::string getDownloadFolder1() const;
	/**
	 * The folder where music is stored
	 * @return Absolute path to the music folder
	 */
	std::string getMusicFolder() const;
	/**
	 * The folder where video is stored
	 * @return Absolute path to the video folder
	 */
	std::string getVideoFolder() const;
	/**
	 * The base folder for storing saved games.
	 * You must add the program name to it like this:
	 * @code{.cpp}
	 * PlatformFolders pf;
	 * string saved_games_folder = pf.getSaveGamesFolder1()+"/My Program Name/";
	 * @endcode
	 * @note Windows: This is an XP compatible version and returns the path to "My Games" in Documents. Vista and later has an official folder.
	 * @note Linux: XDF does not define a folder for saved games. This will just return the same as GetDataHome()
	 * @return The folder base folder for storing save games.
	 */
	std::string getSaveGamesFolder1() const;
private:
	PlatformFolders(const PlatformFolders&);
	PlatformFolders& operator=(const PlatformFolders&);
#if !defined(_WIN32) && !defined(__APPLE__)
	struct PlatformFoldersData;
	PlatformFoldersData* data;
#endif
};

#endif // skip doxygen

}; // namespace PlatformFolders

#endif /* PFOLDERS_CLASS_HPP */
