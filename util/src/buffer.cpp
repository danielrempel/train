#include "include/buffer.hpp"

#include <cstring>

static void own_deleter(unsigned char *data)
{
	delete[] data;
}

Buffer::Buffer()
	: storage(nullptr), count(0), size(0), extension_size(4096)
{
	expand();
}

Buffer::~Buffer()
{

}

size_t
Buffer::length() const
{
	return count;
}

void
Buffer::popCount(std::size_t count)
{
	leftShift(count);
}

std::shared_ptr<unsigned char>
Buffer::rawData()
{
	return storage;
}

void
Buffer::expand()
{
	std::shared_ptr<unsigned char> newdata(new unsigned char[size + extension_size], own_deleter);
	memcpy(newdata.get(), storage.get(), count);
	storage = newdata;
	size = size + extension_size;
}

void
Buffer::leftShift(std::size_t shift)
{
	for (std::size_t index = 0; index + shift < count; index++)
	{
		storage.get()[index] = storage.get()[index + shift];
	}
	if (count < shift)
	{
		count = 0;
	} else {
		count = count - shift;
	}
}
