#include <stdio.h>
#include <stdint.h>

void anim_step(uint64_t *counter, uint64_t step, uint64_t finale)
{
    uint64_t divider = (finale / 1000);
    if (! divider)
    {
        divider = 1;
    }

    if (0 == step % (finale / divider))
    {
        switch (*counter % 4)
        {
            case 0:
                fprintf(stderr, ".");
                break;
            case 1:
                fprintf(stderr, ".");
                break;
            case 2:
                fprintf(stderr, ".");
                break;
            case 3:
                fprintf(stderr, "\r    \r");
                break;
        }

        ++*counter;
        if (4 == *counter)
        {
            *counter = 0;
        }
    }
}

void anim_clean()
{
    fprintf(stderr, "\r");
}
