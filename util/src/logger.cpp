#include <errno.h>
#include <cstdio>
#include <string.h>

#include <chrono>
#include <iomanip>

#include "include/logger.hpp"

#define LOGGER_MESSAGE_BUFFER_SIZE 4096

FILE* __global_logger_output = stderr;

Logger::Logger(const char *tag, LogLevel level)
    : m_tag(tag), m_loglevel(level), m_en_errno(false), m_errno(0)
{}

Logger::~Logger()
{
    m_tag = nullptr;
}

Logger& Logger::EnErrno()
{
    m_en_errno = true;
    m_errno = errno;

    return (*this);
}

void Logger::Trace(const char *fmt, ...)
{
    if (LogLevel::Trace != m_loglevel)
    {
        return;
    }

    va_list args,copy;
    va_start(args, fmt);
    va_copy(copy, args);

    Logger::log(m_en_errno, m_errno, "trace", m_tag, fmt, copy);

    va_end(args);
}

void Logger::Debug(const char *fmt, ...)
{
    if (LogLevel::Debug > m_loglevel)
    {
        return;
    }

    va_list args,copy;
    va_start(args, fmt);
    va_copy(copy, args);

    Logger::log(m_en_errno, m_errno, "debug", m_tag, fmt, copy);

    va_end(args);
}

void Logger::Info(const char *fmt, ...)
{
    if (LogLevel::Info > m_loglevel)
    {
        return;
    }

    va_list args,copy;
    va_start(args, fmt);
    va_copy(copy, args);

    Logger::log(m_en_errno, m_errno, "info", m_tag, fmt, copy);

    va_end(args);
}

void Logger::Warning(const char *fmt, ...)
{
    if (LogLevel::Warning > m_loglevel)
    {
        return;
    }

    va_list args,copy;
    va_start(args, fmt);
    va_copy(copy, args);

    Logger::log(m_en_errno, m_errno, "warn", m_tag, fmt, copy);

    va_end(args);
}

void Logger::Error(const char *fmt, ...)
{
    /*if (LogLevel::Error <= m_loglevel)
    {
        return;
    }*/

    va_list args,copy;
    va_start(args, fmt);
    va_copy(copy, args);

    Logger::log(m_en_errno, m_errno, "error", m_tag, fmt, copy);

    va_end(args);
}

void Logger::log(bool en_errno, int err_no, const char *lvl_tag,
        const char *tag, const char *fmt, va_list args)
{
    static char date_buffer[50] = { 0 };

    static char output_buffer[LOGGER_MESSAGE_BUFFER_SIZE] = { 0 };

    auto now = std::chrono::system_clock::now();
    std::chrono::duration<double> since_the_Epoch = now.time_since_epoch();

    snprintf(date_buffer, 50, "[%.6lf]", since_the_Epoch.count());

    vsnprintf(output_buffer, LOGGER_MESSAGE_BUFFER_SIZE, fmt, args);

    if (en_errno)
    {
        fprintf(__global_logger_output, "%s %s: %s: %s: %s(%d)\n", date_buffer,
                lvl_tag, tag, output_buffer, strerror(err_no), err_no);

        en_errno = false;
        err_no = 0;
    } else {
        fprintf(__global_logger_output, "%s %s: %s: %s\n", date_buffer,
                lvl_tag, tag, output_buffer);
    }

    fflush(__global_logger_output);
}
