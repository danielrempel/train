#include <string.h>
#include <unistd.h>

#include "include/monitor.hpp"
#include "include/logger.hpp"
#include "config.hpp"

Monitor::Monitor(unsigned event_queue_size)
    : m_queue_size(event_queue_size),
        m_events_available(0),
        m_queue(m_queue_size)
{
    Logger logger("Monitor()", UTIL_LOG_LEVEL);

    int fd = epoll_create(/* unused */ 1);
    if (-1 == fd)
    {
        logger.EnErrno().Error("failed to create epoll instance");
        throw std::exception();
    }
    m_epoll = std::move(GenericRAII<int>(fd, [](int fd) { close(fd); }));
}

Monitor::~Monitor()
{}

void Monitor::Add(int fd, unsigned flags)
{
    Logger logger("Monitor::Add()", UTIL_LOG_LEVEL);

    struct epoll_event event_s = { 0 };
    event_s.events = flags;

    if (0 != epoll_ctl(m_epoll.Get(), EPOLL_CTL_ADD, fd, &event_s))
    {
        logger.EnErrno().Error("epoll_ctl() failed");
        throw std::exception();
    }
}

void Monitor::Del(int fd)
{
    Logger logger("Monitor::Del()", UTIL_LOG_LEVEL);

    struct epoll_event event_s = { 0 };

    if (0 != epoll_ctl(m_epoll.Get(), EPOLL_CTL_DEL, fd, &event_s))
    {
        logger.EnErrno().Error("epoll_ctl() failed");
        throw std::exception();
    }
}

void Monitor::Poll(int timeout_ms)
{
    Logger logger("Monitor::Poll()", UTIL_LOG_LEVEL);

    if (0 != m_events_available)
    {
        logger.Error("Events still avaialble");
        throw std::exception();
    }

    memset(m_queue.data(), 0, sizeof(struct epoll_event) * m_queue_size);

    int retval = epoll_wait(m_epoll.Get(), m_queue.data(), m_queue_size,
            timeout_ms);

    if (-1 == retval)
    {
        logger.EnErrno().Error("epoll_wait() failed");
        throw std::exception();
    }

    m_events_available = retval;
}

unsigned Monitor::CountEventsAvailable()
{
    return (m_events_available);
}

int Monitor::PopFD()
{
    if (0 == m_events_available)
    {
        Logger("Monitor::PopFD()", UTIL_LOG_LEVEL)
            .Error("No events available!");
        throw std::exception();
    }

    int ret_fd = m_queue[0].data.fd;

    memmove(m_queue.data(), m_queue.data()+1, sizeof(struct epoll_event) * (m_events_available-1));
    --m_events_available;

    return (ret_fd);
}
