#include <exception>
#include <system_error>
#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <strings.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "include/socket.hpp"
#include "include/logger.hpp"
#include "config.hpp"

Socket::Socket()
	: fd(constructSocket(-1))
{
}

Socket::Socket(const char* addr, int port)
	: fd(constructSocket(-1))
{
    Logger logger("Socket()", UTIL_LOG_LEVEL);

	int tfd = -1;
	if ((tfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		int errsv = errno;
          logger.EnErrno().Error("failed to %s", "create socket");
		throw std::system_error(errsv, std::system_category());
	}

	struct sockaddr_in serverAddressStruct;
	struct hostent *serverHostent = NULL;

	serverHostent = gethostbyname(addr);

	if (NULL == serverHostent)
	{
		int errsv = errno;
          logger.EnErrno().Error("failed to %s", "lookup host");
		throw std::system_error(errsv, std::system_category());
	}

	bzero((char *) &serverAddressStruct, sizeof(serverAddressStruct));
	serverAddressStruct.sin_family = AF_INET;
	bcopy((char *)serverHostent->h_addr,
		(char *)&serverAddressStruct.sin_addr.s_addr,
		serverHostent->h_length);
	serverAddressStruct.sin_port = htons(port);

	if (connect(tfd, (struct sockaddr *)&serverAddressStruct, sizeof(serverAddressStruct)) != 0)
	{
		int errsv = errno;
		close(tfd);
          logger.EnErrno().Error("failed to %s", "connect");
		throw std::system_error(errsv, std::system_category());
	}
	*fd = tfd;
}

Socket::Socket(int newfd)
	: fd(constructSocket(newfd))
{}

/* Copy constructor */
Socket::Socket(const Socket& other)
	: fd(other.fd)
{}

/* Move constructor */
Socket::Socket(Socket&& other) noexcept
{
	fd = other.fd;
	other.fd = std::shared_ptr<int>(nullptr);
}

/* Copy assignment */
Socket& Socket::operator=(const Socket& other)
{
	Socket tmp(other);
	*this = std::move(tmp);
	return *this;
}

Socket& Socket::operator=(Socket&& other) noexcept
{
	if (&other == this)
	{
		return *this;
	}
	fd.reset();
	fd = other.fd;
	other.fd = std::shared_ptr<int>(nullptr);
	return *this;
}

std::size_t
Socket::recv(unsigned char* buffer, std::size_t length)
{
    Logger logger("Socket::recv()", UTIL_LOG_LEVEL);
	if (!isValid())
	{
          logger.EnErrno().Error("failed to %s: %s", "recv", "invalid socket");
		throw std::logic_error(std::string("invalid socket"));
	}
	long int n = read(*fd,buffer,length);
    if (n < 0)
    {
		int errsv = errno;
          logger.EnErrno().Error("failed to %s", "recv");
		throw std::system_error(errsv, std::system_category());
	}
	return (std::size_t)n;
}

std::size_t
Socket::send(const unsigned char* buffer, std::size_t length)
{
    Logger logger("Socket::send()", UTIL_LOG_LEVEL);
	if (!isValid())
	{
          logger.EnErrno().Error("failed to %s: %s", "send", "invalid socket");

		throw std::logic_error(std::string("invalid socket"));
	}
	long int n = write(*fd,buffer,length);
    if (n < 0)
    {
		int errsv = errno;
          logger.EnErrno().Error("failed to %s", "send");
		throw std::system_error(errsv, std::system_category());
	}
	return (std::size_t)n;
}

void
Socket::recvAll(unsigned char* buffer, std::size_t length)
{
	std::size_t rcvd = 0;
	while (rcvd < length)
	{
		rcvd += recv(&(buffer[rcvd]), length - rcvd);
	}
}

void
Socket::sendAll(const unsigned char* buffer, std::size_t length)
{
	std::size_t sent = 0;
	while (sent < length)
	{
		sent += send(&(buffer[sent]), length - sent);
	}
}

int Socket::bytesAvailable() const
{
	int count;
	ioctl(*fd, FIONREAD, &count);
	return count;
}

bool Socket::isValid() const
{
	//std::cout << __PRETTY_FUNCTION__ << ": fd.get(): " << (long long)(fd.get()) << std::endl;
	return (nullptr != fd.get()) && ((*fd) >= 0);
}

int Socket::getFD() const
{
    return (*fd);
}

Socket::~Socket()
{
}

void Socket::DeleteSocket(int *socket)
{
	//std::cout << __PRETTY_FUNCTION__ << ": socket address: " << (long long)(socket) << std::endl;
	//std::cout << __PRETTY_FUNCTION__ << ": closing socket with fd = " << *socket << std::endl;
	if (nullptr != socket)
	{
		if (*socket >= 0)
		{
			close(*socket);
		}
		delete socket;
	}
}

std::shared_ptr<int>
Socket::constructSocket(int *socket)
{
	return std::shared_ptr<int>(socket, Socket::DeleteSocket);
}

std::shared_ptr<int>
Socket::constructSocket(int socket)
{
	int* i = new int;
	(*i) = socket;
	return constructSocket(i);
}
