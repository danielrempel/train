CONF_COMMON_CXXFLAGS+=-Wall -Wextra -pedantic -Werror -Wfatal-errors
CONF_DEBUG_CXXFLAGS+=-DDEBUG -g

CONF_PKG_TYPE=lib

CONF_COMMON_CFLAGS=-std=gnu89 -Wall -Wextra -pedantic
#-Werror -Wfatal-errors
CONF_RELEASE_CFLAGS=-O3
CONF_DEBUG_CFLAGS=-g
CONF_TEST_CFLAGS=-isystem ../ext-includes/ -g -DUNDER_TEST
CONF_CC=gcc

CONF_COMMON_CXXFLAGS=-std=c++17 -isystem ../ext-includes/ -Wall -Wextra -pedantic
#-Werror -Wfatal-errors
CONF_RELEASE_CXXFLAGS=-O3
CONF_DEBUG_CXXFLAGS=-g
CONF_TEST_CXXFLAGS=-isystem ../ext-includes/ -g -DUNDER_TEST
CONF_CXX=g++

CONF_COMMON_LDFLAGS=
CONF_RELEASE_LDFLAGS=
CONF_DEBUG_LDFLAGS=
CONF_TEST_LDFLAGS=../ext-libs/libgtest.a ../ext-libs/libgtest_main.a -lpthread

CONF_LD=${CONF_CXX}

CONF_AR=ar
CONF_AR_FLAGS=rcs

CONF_DEPS=
