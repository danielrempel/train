#ifndef __HAVE_SOCKETTEMPLATES_HPP__
#define __HAVE_SOCKETTEMPLATES_HPP__

template<class T>
T
Socket::readType()
{
	unsigned char buffer[sizeof(T)];
	T val;
	recvAll(buffer,sizeof(T));
	unsigned char *chVal = (unsigned char*)&val;
	for(unsigned int i=0; i<sizeof(T); i+=1)
	{
		*(chVal++) = buffer[i];
	}
	return val;
}

template<class T>
void
Socket::writeType(const T& val)
{
	unsigned char buffer[sizeof(T)];
	unsigned char *chVal = (unsigned char*)&val;
	for(unsigned int i=0; i<sizeof(T); i+=1)
	{
		buffer[i] = *(chVal++);
	}
	sendAll(buffer,sizeof(T));
}

#endif
