#ifndef UTIL_GATHER_HPP
#define UTIL_GATHER_HPP

template<typename It1, typename It2, typename It3>
void gather(It1 perm_begin, It1 perm_end, It2 source, It3 destination)
{
    for (auto it = perm_begin; it != perm_end; ++it)
    {
        *(destination + std::distance(perm_begin, it)) = *(source + *it);
    }
}

template<typename T1, typename T2, typename It3>
void gather(T1* perm_begin, T1* perm_end, T2* source, It3 destination)
{
    for (auto it = perm_begin; it != perm_end; ++it)
    {
        *(destination + std::distance(perm_begin, it)) = *(source + *it);
    }
}

#endif /* UTIL_GATHER_HPP */
