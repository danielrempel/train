#ifndef UTIL_MONITOR_HPP
#define UTIL_MONITOR_HPP

#include <sys/epoll.h>

#include <vector>

class Monitor;

#include "generic_raii.hpp"

class Monitor final
{
public:
    Monitor(unsigned event_queue_size = 10);
    ~Monitor() noexcept;

    void Add(int fd, unsigned flags);
    void Del(int fd);

    /* timeout_ms :
     *  -1 : indefinite block
     *  0  : immediate return
     *  >0 : about timeout_ms ms block
     */
    void Poll(int timeout_ms);

    unsigned CountEventsAvailable();
    int PopFD();

private:
    GenericRAII<int> m_epoll;

    unsigned const m_queue_size;
    unsigned m_events_available;
    std::vector<struct epoll_event> m_queue;
};

#endif /* UTIL_MONITOR_HPP */
