/* ========================================================================= */
/*
 * Generic RAII wrapper class
 *
 * Daniel Rempel <danil.rempel@gmail.com>
 *
 * 2019
 */
/* ========================================================================= */
#ifndef LIBUTIL_GENERIC_RAII_HPP
#define LIBUTIL_GENERIC_RAII_HPP

#include <functional>

template<typename T>
class GenericRAII;

/* local includes here */

/* ========================================================================= */
template<typename T>
class GenericRAII final
{
public:
    GenericRAII();
    GenericRAII(T value, std::function<void(T)> func);
    ~GenericRAII();

    GenericRAII(GenericRAII<T> const&) = delete;
    GenericRAII<T>& operator=(GenericRAII<T> const&) = delete;

    GenericRAII(GenericRAII<T>&& other);
    GenericRAII<T>& operator=(GenericRAII<T>&& other);

    /* Get the managed value */
    T Get() const;

    /* Return the managed value, stop managing it and empty the GenericRAII */
    T Unwrap();

    void Reset();

private:
    T m_value;
    std::function<void(T)> m_dtor;

    void FreeContents();

    static void EmptyDeleter(T const& unused);
};

/* ========================================================================= */
template<typename T>
GenericRAII<T>::GenericRAII()
    : m_dtor(EmptyDeleter)
{}

/* ========================================================================= */
template<typename T>
GenericRAII<T>::GenericRAII(T value, std::function<void(T)> func)
    : m_value(value), m_dtor(func)
{}

/* ========================================================================= */
template<typename T>
GenericRAII<T>::~GenericRAII()
{
    FreeContents();
}

/* ========================================================================= */
template<typename T>
GenericRAII<T>::GenericRAII(GenericRAII<T>&& other)
    : GenericRAII<T>()
{
    *this = std::move(other);
}

/* ========================================================================= */
template<typename T>
GenericRAII<T>& GenericRAII<T>::operator=(GenericRAII<T>&& other)
{
    FreeContents();

    m_value = other.m_value;
    m_dtor = other.m_dtor;

    other.m_value = T();
    other.m_dtor = EmptyDeleter;

    return (*this);
}

/* ========================================================================= */
template<typename T>
void GenericRAII<T>::FreeContents()
{
    m_dtor(m_value);
    m_value = T();
    m_dtor = EmptyDeleter;
}

/* ========================================================================= */
template<typename T>
T GenericRAII<T>::Get() const
{
    return (m_value);
}

/* ========================================================================= */
template<typename T>
T GenericRAII<T>::Unwrap()
{
    T retval = m_value;

    m_value = T();
    m_dtor = EmptyDeleter;

    return (retval);
}

/* ========================================================================= */
template<typename T>
void GenericRAII<T>::Reset()
{
    FreeContents();
}

/* ========================================================================= */
template<typename T>
void GenericRAII<T>::EmptyDeleter(T const& unused)
{}

#endif /* LIBUTIL_GENERIC_RAII_HPP */
