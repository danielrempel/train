#ifndef HAVE_LOGGER_HPP
#define HAVE_LOGGER_HPP

class Logger;
enum class LogLevel;

#include <cstdarg>

enum class LogLevel
{
    Error,
    Warning,
    Info,
    Debug,
    Trace,
};

class Logger final
{
public:
    Logger(const char *tag, LogLevel level);
    ~Logger();

    Logger& EnErrno();

    void Trace(const char *fmt, ...);
    void Debug(const char *fmt, ...);
    void Info(const char *fmt, ...);
    void Warning(const char *fmt, ...);
    void Error(const char *fmt, ...);

protected:
private:
    char const* m_tag;
    LogLevel m_loglevel;
    bool m_en_errno;
    int m_errno;

    static void log(bool en_errno, int err_no, const char *lvl_tag,
            const char *tag, const char *fmt, va_list args);
};

#endif // HAVE_LOGGER_HPP
