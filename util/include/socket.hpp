#ifndef __HAVE_SOCKET_HPP__
#define __HAVE_SOCKET_HPP__
class Socket;

#include <cstddef>
#include <memory>

/*
 * Acts as shared_ptr<unix_socket>
 */
class Socket
{
	public:
		Socket();
		Socket(const char* addr, int port);
		Socket(int newfd);
		Socket(const Socket& other);
		Socket(Socket&& other) noexcept;

		Socket& operator=(const Socket& other);
		Socket& operator=(Socket&& other) noexcept;

          /* Blocks, may be interrupted on signal */
		std::size_t recv(unsigned char* buffer, std::size_t length);
		std::size_t send(const unsigned char* buffer, std::size_t length);

          /* Blocks */
		void recvAll(unsigned char* buffer, std::size_t length);
		void sendAll(const unsigned char* buffer, std::size_t length);

          /* Awful API. I made it a couple of years ago, sorry */
          /* May return negative on error */
		int bytesAvailable() const;

		bool isValid() const;

          int getFD() const;

		template<class T>
		T readType();
		template<class T>
		void writeType(const T& val);

		virtual ~Socket();

		static void DeleteSocket(int *socket);
	protected:
		std::shared_ptr<int> fd;
	private:
		std::shared_ptr<int> constructSocket(int socket);
		std::shared_ptr<int> constructSocket(int *socket);
};

#endif // __HAVE_SOCKET_HPP__
#include "socket-templates.hpp"
