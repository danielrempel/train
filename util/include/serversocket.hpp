#ifndef __HAVE_SERVERSOCKET_HPP__
#define __HAVE_SERVERSOCKET_HPP__
class ServerSocket;

#include "socket.hpp"

class ServerSocket : public Socket
{
	public:
		// Note: doesn't really use addr, but I have no ideas on how
		// to distinguish between first two constructors better
		ServerSocket(const char* addr, int port);
		ServerSocket(int newfd);
		ServerSocket(const ServerSocket& other);
		ServerSocket(ServerSocket&& other) noexcept;
		virtual ~ServerSocket();

		ServerSocket& operator=(const ServerSocket& other);
		ServerSocket& operator=(ServerSocket&& other) noexcept;

		void listen(int queueLength);
		Socket accept();
		bool isClientInQueue();

	protected:
	private:
};

#endif // __HAVE_SERVERSOCKET_HPP__
