#ifndef HAVE_BUFFER_HPP
#define HAVE_BUFFER_HPP
class Buffer;

#include <memory>
#include <cstddef>

class Buffer
{
public:
    Buffer();
    virtual ~Buffer();

    size_t length() const;

    template<class T>
        void appendType(const T& val)
        {
            if (size - count < sizeof(T))
            {
                expand();
            }
            unsigned char *chVal = (unsigned char*)&val;
            for(unsigned int i=0; i<sizeof(T); i+=1)
            {
                storage.get()[count++] = ( *(chVal++) );
            }
        }

    template<class T>
        T popType()
        {
            T val;
            unsigned char *chVal = (unsigned char*)&val;
            for(unsigned int i=0; i<sizeof(T); i+=1)
            {
                *(chVal++) = storage.get()[i];
            }
            leftShift(sizeof(T));
            return val;
        }

    void popCount(std::size_t count);
    std::shared_ptr<unsigned char> rawData();
protected:
private:
    std::shared_ptr<unsigned char> storage;
    // count means current stored data amount
    // size is total current size
    std::size_t count, size;
    std::size_t extension_size;

    void expand();
    void leftShift(std::size_t shift);
};

#endif // HAVE_BUFFER_HPP
