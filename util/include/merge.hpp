#include <cstdint>
#include <cstdlib>
#include <cassert>
#include <cstring>

#include <algorithm>

#define MIN(A,B) ((A) < (B) ? (A) : (B))

#define SORT_SMALL_MAXCHUNK (512)

template<typename T>
void sort_small(T* array, uint64_t len);

template<typename T>
void merge(T* src1, T* src2, T* dest, uint64_t len_1, uint64_t len_2);

template<typename T>
int merge_sort(T* array, uint64_t const len)
{
    uint64_t index = 0;
    uint64_t step = 0;

    T *arr_a = NULL, *arr_b = NULL;
    T *tmp_ptr = NULL;
    T *temp = NULL;

    assert(NULL != array);

    temp = (T*) malloc(sizeof(array[0]) * len);
    if (NULL == temp)
    {
        return (1);
    }

    // Sort
    if (len > SORT_SMALL_MAXCHUNK)
    for (index = 0;
            index < len - SORT_SMALL_MAXCHUNK;
            index += SORT_SMALL_MAXCHUNK)
    {
        sort_small(array + index, SORT_SMALL_MAXCHUNK);
    }

    //if (len - index > 1)
    if (len % SORT_SMALL_MAXCHUNK)
    {
        sort_small(array + (len / SORT_SMALL_MAXCHUNK) * SORT_SMALL_MAXCHUNK, len % SORT_SMALL_MAXCHUNK);
    }

    // Merge
    arr_a = array;
    arr_b = temp;

    for (step = SORT_SMALL_MAXCHUNK; step < len; step *= 2)
    {
#pragma omp parallel for
        for (index = 0; index < len; index += 2*step)
        {
            uint64_t t_len = MIN(2*step, len-index);
            uint64_t m_len_1 = t_len/2;
            uint64_t m_len_2 = t_len/2 + t_len % 2;

            merge(arr_a + index, arr_a + index + m_len_1, arr_b + index,
                    m_len_1, m_len_2);
        }

        tmp_ptr = arr_a;
        arr_a = arr_b;
        arr_b = tmp_ptr;
        tmp_ptr = NULL;
    }

    if (arr_a != array && len > SORT_SMALL_MAXCHUNK)
    {
        memcpy(array, temp, sizeof(array[0]) * len);
    }

    free(temp);
    temp = NULL;
    arr_a = NULL;
    arr_b = NULL;

    return (0);
}

template<typename T>
void merge(T* src1, T* src2, T* dest, uint64_t len_1, uint64_t len_2)
{
    uint64_t i_1 = 0, i_2 = 0, i_d = 0;

    while (i_1 < len_1 && i_2 < len_2)
    {
        if (src1[i_1] > src2[i_2])
        {
            dest[i_d] = src1[i_1];
            ++i_1;
        } else {
            dest[i_d] = src2[i_2];
            ++i_2;
        }
        ++i_d;
    }

    for (; i_1 < len_1; ++i_1, ++i_d)
    {
        dest[i_d] = src1[i_1];
    }

    for (; i_2 < len_2; ++i_2, ++i_d)
    {
        dest[i_d] = src2[i_2];
    }
}

template<>
void sort_small(PackedString* array, uint64_t len)
{
    void *nullarg = NULL;

    assert(len <= SORT_SMALL_MAXCHUNK);
    assert(len > 0);

    std::sort(array, array + len);
}
