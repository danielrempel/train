#ifndef PAYLOAD_ANIMATION_H
#define PAYLOAD_ANIMATION_H

#include <stdint.h>

extern "C"
{
    void anim_step(uint64_t *counter, uint64_t step, uint64_t finale);

    void anim_clean();
};

#endif /* PAYLOAD_ANIMATION_H */
