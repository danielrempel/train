#ifndef DFILEPARSER_YYPAYLOAD_HPP
#define DFILEPARSER_YYPAYLOAD_HPP

#include <string>
#include <functional>

#include "location_type.hpp"

struct location_type_t;
typedef void* yyscan_t;

struct YYPayload
{
    using ErrorCallback = void(location_type_t const*, yyscan_t, std::string const& error_message);
    using TargetCallback = void(std::string&& target);
    using DependencyCallback = void(std::string&& dependency);

    std::function<ErrorCallback> error_callback;
    std::function<TargetCallback> target_callback;
    std::function<DependencyCallback> dependency_callback;
};


#endif /* DFILEPARSER_YYPAYLOAD_HPP */
