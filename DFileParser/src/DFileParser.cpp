#include "include/DFileParser.hpp"

#include <memory>
#include "fmt/format.hpp"
#include "util/generic_raii.hpp"

#ifdef PACKAGE_UNDER_TEST
#include "gtest/gtest.h"
#endif /* PACKAGE_UNDER_TEST */

#include "Parser.hpp"

int yylex_init(yyscan_t*);
int yylex_destroy(yyscan_t);
void yyset_in(FILE*, yyscan_t);

namespace fs = std::filesystem;

static void SetFile(fs::path const&, yyscan_t, GenericRAII<FILE*>& out_file);

/* ========================================================================= */
DFileParser::DFileParser(fs::path const& file, std::string const& object)
    : m_file(file), m_object(object)
{
    ScanDeps();
}

/* ========================================================================= */
void DFileParser::ScanDeps()
{
    using namespace std::placeholders;

    GenericRAII<FILE*> file_handle;
    GenericRAII<yyscan_t> scanner;

    yyscan_t temp;
    if (0 != yylex_init(&temp))
    {
        throw std::runtime_error("yylex_init() failed");
    }

    scanner = GenericRAII<yyscan_t>(temp, [](yyscan_t s) { yylex_destroy(s); });

    SetFile(m_file, scanner.Get(), file_handle);

    YYPayload payload = YYPayload {
        std::function<YYPayload::ErrorCallback>(
                [](location_type_t const* l, yyscan_t, std::string const& error_message) -> void
                {
                    fmt::print("{} in line {}\n", error_message, l->first_line);
                    throw std::runtime_error("bison error!");
                }
        ),
        std::function<YYPayload::TargetCallback>(std::bind(&DFileParser::OnTarget, this, _1)),
        std::function<YYPayload::DependencyCallback>(std::bind(&DFileParser::OnDependency, this, _1))
    };

    yyparse(scanner.Get(), payload);
}

void DFileParser::OnTarget(std::string&& target)
{
    m_current_target = std::move(target);
}

void DFileParser::OnDependency(std::string&& dependency)
{
    m_deps[m_current_target].emplace_back(std::move(dependency));
}

/* ========================================================================= */
/* static */ void SetFile(fs::path const& file_path, yyscan_t scanner,
        GenericRAII<FILE*>& out_file)
{
    out_file.Reset();
    out_file = GenericRAII<FILE*>(
            fopen(file_path.string().c_str(), "r"),
            [](FILE* f) { fclose(f); }
        );
    if (nullptr == out_file.Get())
    {
        throw std::runtime_error("fopen() failed");
    }

    yyset_in(out_file.Get(), scanner);
}

#ifdef PACKAGE_UNDER_TEST
/* ========================================================================= */
namespace {

TEST(DFileParserTest, build_debug_Lexerd)
{
    auto file = fs::path("build/debug/Lexer.d");

    auto parser = DFileParser(file, "Lexer.o");

    std::list<std::string> result;

    ASSERT_NO_THROW(result = parser.GetDeps("build/debug/Lexer.o"));

    result.sort();

    std::list<std::string> reference {
        "build/debug/Lexer.cpp", "src/../src/location_type.hpp",
        "src/../build/debug/Parser.hpp", "src/../src/yyerror_callback.hpp",
        "src/../src/location_type.hpp"
    };
    reference.sort();

    //EXPECT_EQ(result, reference);
}

} // namespace anonymous
#endif /* PACKAGE_UNDER_TEST */
