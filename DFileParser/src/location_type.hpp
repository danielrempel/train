#ifndef DFILEPARSER_LOCATION_TYPE_HPP
#define DFILEPARSER_LOCATION_TYPE_HPP

struct location_type_t
{
    unsigned first_line;
    unsigned first_column;
    unsigned last_line;
    unsigned last_column;
};

#define YYLTYPE location_type_t

#endif /* DFILEPARSER_LOCATION_TYPE_HPP */
