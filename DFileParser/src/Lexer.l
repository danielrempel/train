%{

#include <string>

#include "fmt/format.hpp"

#include "src/location_type.hpp"
#include "Parser.hpp"

/* update location on matching */
#undef YY_NEW_FILE
#define YY_NEW_FILE
#undef YY_USER_ACTION
#define YY_USER_ACTION { \
    yylloc->first_line = yylloc->last_line; \
    yylloc->last_line = yylineno; }

int yylex(YYSTYPE* yylval_param, location_type_t* yylloc_param,
    yyscan_t yyscanner);

%}

%option nodefault
%option noyywrap
%option yylineno
%option bison-locations bison-bridge
%option reentrant

%%

[A-Za-z0-9.,_/-]*     {
                        *yylval = yytext;
                        return TOKEN_IDENT;
                    }

\"([^\n\"]|\\\")*\" {
                        *yylval = yytext;
                        return TOKEN_IDENT;
                    }


:                   {
                        return TOKEN_COLON;
                    }

\\\n                {
                        return TOKEN_ESC_NEWLINE;
                    }

\n                  {
                        return TOKEN_NEWLINE;
                    }

[ ]                 {
                        return TOKEN_SPACE;
                    }

[\t]                 {
                        return TOKEN_OTHER_WHSPACE;
                    }

.                   {
                        fmt::print("?? 0x{:02X} '{}'\n", yytext[0], yytext);
                        throw std::runtime_error("Unexpected character input");
                    }

%%
