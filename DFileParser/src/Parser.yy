%defines
%locations
%define parse.assert
%define api.value.type {std::string}
%define api.pure full
%define api.token.prefix {TOKEN_}
%lex-param {yyscan_t scanner}
%parse-param {yyscan_t scanner}
%parse-param {YYPayload& payload}

%token IDENT;
%token COLON;
%token ESC_NEWLINE;
%token NEWLINE;
%token SPACE;
%token OTHER_WHSPACE;

%code top
{
    #include <string>
    #include "fmt/format.hpp"

    #include "src/location_type.hpp"
}
%code requires
{
    #include "src/YYPayload.hpp"
    typedef void* yyscan_t;
}
%code
{
    void yyerror(location_type_t const* l, yyscan_t scanner, YYPayload& payload, std::string const& err_message);
    int yylex(YYSTYPE* yylval, location_type_t* yylloc, yyscan_t scanner);
}

%%

file
    : %empty
    | target
    | file_whitespace
    | file target
    | file file_whitespace
    ;

file_whitespace
    : SPACE
    | OTHER_WHSPACE
    | NEWLINE
    ;

target
    : IDENT { payload.target_callback(std::string($1)); }
        opt_whitespace COLON opt_whitespace deplist NEWLINE
    ;

deplist
    : %empty
    | IDENT { payload.dependency_callback(std::string($1)); }
    | deplist deplist_space IDENT { payload.dependency_callback(std::string($3)); }
    ;

deplist_space
    : deplist_space_itm
    | deplist_space deplist_space_itm
    ;

deplist_space_itm
    : SPACE
    | OTHER_WHSPACE
    | ESC_NEWLINE
    ;

opt_whitespace
    : %empty
    | SPACE opt_whitespace
    | OTHER_WHSPACE opt_whitespace
    ;

%%

void yyerror(location_type_t const* l, yyscan_t scanner, YYPayload& payload, std::string const& err_message)
{
    payload.error_callback(l, scanner, err_message);
}
