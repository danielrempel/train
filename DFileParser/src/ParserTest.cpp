#include <list>
#include <string>
#include <map>
#include <filesystem>
#include <memory>
#include "fmt/format.hpp"
#include "util/generic_raii.hpp"

#ifdef PACKAGE_UNDER_TEST
#include "gtest/gtest.h"
#endif /* PACKAGE_UNDER_TEST */

#include "Parser.hpp"

int yylex_init(yyscan_t*);
int yylex_destroy(yyscan_t);
void yyset_in(FILE*, yyscan_t);

namespace fs = std::filesystem;

#ifdef PACKAGE_UNDER_TEST
/* ========================================================================= */
namespace {

// The fixture for testing class Foo.
class ParserTest : public ::testing::Test
{
public:
    ParserTest();
    virtual ~ParserTest() noexcept;

    void SetFile(fs::path const& file);
    void SetFileToString(std::string&& value);

    yyscan_t GetScanner();

    YYPayload& GetPayload();

private:
    std::string m_file_string;

    GenericRAII<FILE*> m_file;
    GenericRAII<yyscan_t> m_scanner;

    YYPayload m_payload;

    void OnError(location_type_t const* l, yyscan_t, std::string const& error_message);
    void OnTarget(std::string&& target);
    void OnDependency(std::string&& dependency);
};

ParserTest::ParserTest()
{
    using namespace std::placeholders;

    yyscan_t temp;
    yylex_init(&temp);
    m_scanner = GenericRAII<yyscan_t>(temp, [](yyscan_t s) { yylex_destroy(s); });

    m_payload = YYPayload {
        std::function<YYPayload::ErrorCallback>(std::bind(&ParserTest::OnError, this, _1, _2, _3)),
        std::function<YYPayload::TargetCallback>(std::bind(&ParserTest::OnTarget, this, _1)),
        std::function<YYPayload::DependencyCallback>(std::bind(&ParserTest::OnDependency, this, _1))
    };
}

ParserTest::~ParserTest() noexcept
{}

void ParserTest::SetFile(fs::path const& file)
{
    m_file.Reset();
    m_file_string = file.string();
    m_file = GenericRAII<FILE*>(
            fopen(m_file_string.c_str(), "r"),
            [](FILE* f) { fclose(f); }
        );
    if (nullptr == m_file.Get())
    {
        throw std::runtime_error("fopen() failed");
    }

    yyset_in(m_file.Get(), m_scanner.Get());
}

void ParserTest::SetFileToString(std::string&& value)
{
    // std::string::c_str() ptr is formally invalidated by (almost) any
    // non-const call
    //
    //
    // We have to first destroy the old m_file
    // Then set the m_file_string
    // Then create a new m_file
    //

    m_file.Reset();

    m_file_string = std::move(value);

    m_file = GenericRAII<FILE*>(
            fmemopen(const_cast<char*>(m_file_string.c_str()),
                        m_file_string.length(), "r"),
            [](FILE* f) { fclose(f); }
        );
    if (nullptr == m_file.Get())
    {
        throw std::runtime_error("fmemopen() failed");
    }

    yyset_in(m_file.Get(), m_scanner.Get());
}

yyscan_t ParserTest::GetScanner()
{
    return (m_scanner.Get());
}

YYPayload& ParserTest::GetPayload()
{
    return (m_payload);
}

void ParserTest::OnError(location_type_t const* l, yyscan_t,
        std::string const& error_message)
{
    fmt::print("{} in line {}\n", error_message, l->first_line);
    throw std::runtime_error("bison error!");
}

void ParserTest::OnTarget(std::string&& target)
{
    fmt::print("Target: '{}'\n", target);
}

void ParserTest::OnDependency(std::string&& dependency)
{
    fmt::print("Dependency: '{}'\n", dependency);
}

/* ========================================================================= */
TEST_F(ParserTest, build_debug_Lexer_d)
{
    SetFile(fs::path("build/debug/Lexer.d"));

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, target_no_deps)
{
    SetFileToString("target:\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, target_one_dep)
{
    SetFileToString("target: dep1\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, target_two_deps)
{
    SetFileToString("target: dep1 dep2\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, target_deps_newline)
{
    SetFileToString("target: dep1 \\\ndep2\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, multiple_targets)
{
    SetFileToString("target1: dep1_1 \\\ndep1_2\n\
                         \ntarget2: dep2_1 dep2_2\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, target_with_slash)
{
    SetFileToString("target/foo: dep1_1\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, dependency_with_slash)
{
    SetFileToString("target: dep/1_1\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, quoted_target)
{
    SetFileToString("\"target\": dep1_1\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, quoted_dep)
{
    SetFileToString("target: \"dep1_1\"\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

TEST_F(ParserTest, quoted_dep_space)
{
    SetFileToString("target: \"dep1 1\"\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

/*
TEST_F(ParserTest, utf8_target)
{
    SetFileToString("цель: dep1\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}
*/

TEST_F(ParserTest, quoted_utf8_target)
{
    SetFileToString("\"מתרה\": dep1\n");

    ASSERT_NO_THROW(yyparse(GetScanner(), GetPayload()));
}

}
#endif /* PACKAGE_UNDER_TEST */
