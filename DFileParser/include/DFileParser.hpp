#ifndef TRAIN_COMPILATION_DFILEPARSER_HPP
#define TRAIN_COMPILATION_DFILEPARSER_HPP

class DFileParser;

#include <list>
#include <string>
#include <map>
#include <filesystem>

class DFileParser final
{
public:
    DFileParser(std::filesystem::path const& file, std::string const& object);
    ~DFileParser() noexcept = default;

    std::list<std::filesystem::path>& GetDeps();

private:
    void ScanDeps();

    void OnTarget(std::string&& target);
    void OnDependency(std::string&& dependency);

private:
    std::filesystem::path const& m_file;
    std::string const& m_object;

    std::map<std::string, std::list<std::filesystem::path>> m_deps;

    std::string m_current_target;
};

inline std::list<std::filesystem::path>& DFileParser::GetDeps()
{
    return (m_deps.at(m_object));
}

#endif /* TRAIN_COMPILATION_DFILEPARSER_HPP */
