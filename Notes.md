# Development path

* [+] Compilation C++
* [+] Linking a single binary
* [+] Compilation flags and language standard
* [+] Linking flags
* [+] Build type: shared lib
* [+] Build type: static lib
* [+] Basic folder structure
* [+] Dependency checking
* [ ] Dependency list generation&support for .c,.cpp files
* [+] Select target from package type
* [ ] Build modes : release, debug, ??
* [ ] Folder structure : debug vs release
* [+] Package dependencies: tree
* [+] Package dependencies: build them
* [+] Package dependencies: link/include them
* [ ] Target: clean

# Features

* [+] Compilation : C++
* [+] Linking (single binary)
* [+] Compilation flags : C++
* [+] Package configuration file
* [+] Global configuration file
* [ ] Build types/modes: debug, release, custom ones
* [+] Custom build steps
* [+] Global cpp flags
* [+] Overrides
* [+] Dependencies
*  * [+] Build
*  * [+] Link
*  * [+] Include dirs linking
*  * [+] Dependency flags
* [+] Multiple target binaries
* [ ] Features
* [ ] Per-compiler flags

# Package configuration file

Note: afterwards is to be expanded to workspace configuration file

Intended use: package per library/final application or product which may
consist of multiple executable files

# Global configuration file

* [+] processors (array of tables)
*  *  [+] name
*  *  [+] input type
*  *  [+] output type
*  *  [+] format string
*  *  [+] command
*  *  [+] input is source
*  *  [+] flags

# Package configuration

*  * package
*  *  [+] name
*  *  [+] type: { bin, lib, so }
* [+] processors (array of tables)
*  *  [+] name
*  *  [+] input type
*  *  [+] output type
*  *  [+] format string
*  *  [+] command
*  *  [+] input is source
*  *  [+] flags
* [+] file (array of tables)
*  * [+] file
*  * [+] (processor) name
*  * [+] add flags (array)
*  * [+] remove flags (array)
* [ ] dependencies (table of tables)
*  * [+] package name - the name of the table
*  * [ ] features (array)
*  * [ ] registry
*  * [ ] version
*  * [ ] path
*  * [ ] git
*  * [ ] branch
*  * [ ] tag

# Package structure

Supposed package structure would be:

```
train.toml
train.lock
src/
  |- any source file
  |- main.cpp
  |- bin/
  |    |- any source file
  |- any other directory
      |- any directory or source file (recursively)
```

When the build system finds `src/main.cpp` it should be compiled and linked into
`${package_name}` binary file.
When the build system finds `src/bin/*.cpp` it should be compiled and linked into
binary file with name corresponding to the original source file name.
Any other source files should be compiled and linked into a single static library
that would be linked to all the binary files.

When building a static library - nothing changes, all the files (except for
bin sources) are to be linked together into a single static library.

When building a shared library - everything changes, all the files (except for
bin sources) should be compiled with -fPIC and linked into a shared library.

# Conventions

Some things should be defined and enforced, for example header paths in 
libraries, because letting every package decide on its own structure would 
mean that packages made under other conventions won't be able to use them.

Headers and paths convention:

 * External interface headers go to `package_root/include`. From inside the
	package these should be accessed as:
	`package_root/include/foo/bar.hpp` -> `#include "foo/bar.hpp"
 * Internal headers should go to `package_root/src`

# Targets!

Hardcoded as of now.

 * all objects
     both `other_sources` and `binary_sources`
     `CXX` processor output type is sought
 * static lib
     `other_sources` only
     `AR` output type is sought
     build/<package_name>.a
 * all binaries
     static lib target
     `binary sources` are compiled to `LD` output type
     binary objects are linked with `LD` into binaries
 * shared objects
     `other_sources` are compiled with `CXX-shared`
 * shared lib
     shared objects target
     shared lib is build with `LD-shared`
    build/<package_name>.so

Every file present under src/ and matching one of source type patterns would
finally become an object (.o) or a "shared object" (.sharedo) in case of
shared library targets
Some of these files would then become a static/shared lib
Some of these files would then become binaries

# Transformation tools

Processor { source type, dest type }

Defaults:
 * Compile shared C++ { cpp, sharedo }
 * Compile C++ { cpp, o }
 * Link { o, <> }
 * Link shared { sharedo, so }
 * Archive { o, a }

Processor definition in package definition file may be partial when the global
configuration defines a processor with the same name. In such case a new
processor will be created from merging the two definitions. For merge process
definitions see `src/processor.hpp`

# Object dependencies

Each processor is given a list of sources and a destination file. Basic
algorithm would be to check whether the destination file:

 * Exists
 * Is newer than its sources

If there are no sources, we should believe that there's no need to regenerate
an existing file (e.g. mkdir processor)

Afterwards we should somehow support `make`-s dependency files generated by
GCC: we should be able to specify additional flags to generate them and support
using them along with object's sources list.

One option would be to consider them a special case and add special treatment,
the other option would be to treat .d files as an ordinary thing and look for
them by default regardless of processor type.

# Package versions and names

Proposal for requirement on library packages: all the symbols must be hidden
behind a major vesrion namespace.

Example:

```
void vendor::package::version::foo();
```

It makes code and accessing it clumsy, but dependency versioning is going to be
quite clumsy and complicated itself. See **Package dependencies**

Package versioning model - semver 2.0.0

# Package dependencies

Package may depend on another package only if the dependency is a "lib" or
"so".

Due to language and environment limitations, there won't be a way of using
different versions of the same package within the dependency tree. This
limitation may be circumvented for major versions by employing strict
namespace naming rules, support from `train` and avoiding any C symbol
definitions within the dependency package.

In light of the limitations and the nature of the language, any package
used several times within the dependency tree will be resolved to a version
that matches all the requirements of all the depending packages simultaneously.

Failure to find an appropriate version would lead to dependency resolution
failure and will block any further actions.

Additional question arising from language specifics is the question of
compilation flags' strictness which may differ in incompatible fashion between
the dependant and the dependency.

# Compilation flag transformation

Some compilation flags may require paths like package root directory.

`src/flag_transform_callback.hpp:FlagTransformCallback()` is handling that.

It is called in the last moment, when the final compilation flag list is
gathered for a certain file and this callback is using `fmt::format()` to
replace certain predefined variables with their values.

Currently, the following variables are suppored:

 * `package_root`
 * `build_include_dir`

Usage example:

```
flags = [ "-isystem{build_include_root:}" ]
```

# Implementation details

 * FileFinder knows `main*` and `bin/*` as hardcoded, may wish to change that
 * `Target*()` methods have their processors hardcoded

## Workspace entity

Contains:
 * base dir
 * build dir (configuration item, relative)
 * base processors list
 * base source types list
 * List of child packages (as package entities)
 * List of dependencies (as package entities)

Upon adding a package to a workspace (a child package or a dependency),
new dependencies would be resolved and added to the list of dependencies

## Package entity

Contains:
 * base dir
 * custom processors
 * custom source types
 * direct dependencies as package names

*NB: May wish to look for iterator chaining to implement easy searching in
lists of custom and base processors.*

Should search package's processors before searching the base ones. This way
we can facilitate custom processors with minimal effort.
Per file flags should be facilitated using Package's custom processors.

## Package dependencies

A package declares that it requires a certain package

Possible definitions:

```
[dependencies]
foo = "version"
bar = { version = "version", ... }

[dependencies.baz]
version = "version"
```

Possible keys in dependency table:

 * [+] path : dependency path relative to the manifest it is specified in

## Package entity creation

Upon creation, package manifest (`train.toml`) is read and all the data is
filled in the entity. Dependencies are stored only as a list of package names
